import { Component } from '@angular/core';
import { Platform,IonicApp, App, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { LearningTabPage } from '../pages/learning-tab/learning-tab';
import { MandatoryTrainigsPage } from '../pages/mandatory-trainigs/mandatory-trainigs';
import { PastDueTrainingsPage } from '../pages/past-due-trainings/past-due-trainings';
import { ActiveTrainigsPage }  from '../pages/active-trainigs/active-trainigs';
import { ChartsModule } from 'ng2-charts';
import { Navbar } from 'ionic-angular';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { Logger} from 'angular2-logger/core';
import {AppDashboardPage} from '../pages/app-dashboard/app-dashboard';
import { Deeplinks } from '@ionic-native/deeplinks';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;

  constructor(private _app: App, private ionicApp: IonicApp, private _menu: MenuController,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private _logger: Logger ) {
    
    // if (oauthService.hasValidIdToken()) {
    //   this.rootPage = AppDashboardPage;
    // } else {
    //   this.rootPage = LoginPage;
    // }

    // this._logger.error('This is a priority level 1 error message...');
 		// this._logger.warn('This is a priority level 2 warning message...');
 		// this._logger.info('This is a priority level 3 warning message...');
 		// this._logger.debug('This is a priority level 4 debug message...');
    //  this._logger.log('This is a priority level 5 log message...');
    //  //this._logger.level = _logger.Level.LOG(2);
    //  this._logger.store();
    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  
}
