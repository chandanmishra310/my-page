import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginPage } from '../pages/login/login';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { AppDashboardPage } from '../pages/app-dashboard/app-dashboard';
import { WorkHistoryPage } from '../pages/work-history/work-history';
import { ElBalancePage } from '../pages/el-balance/el-balance';
import { LearningTabPage } from '../pages/learning-tab/learning-tab';
import { MandatoryTrainigsPage } from '../pages/mandatory-trainigs/mandatory-trainigs';
import { PastDueTrainingsPage } from '../pages/past-due-trainings/past-due-trainings';
import { ActiveTrainigsPage } from '../pages/active-trainigs/active-trainigs';
import { ChartsModule } from 'ng2-charts';
import { TabsPage } from '../pages/tabs/tabs';
import { BasicDetailsPage } from '../pages/basic-details/basic-details';
import { PersonalDetailsPage } from '../pages/personal-details/personal-details';
import { HomePage } from '../pages/home/home';
import { MangerViewPage } from '../pages/manger-view/manger-view';
import { Navbar } from 'ionic-angular';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { Logger } from 'angular2-logger/core';
import { RestApiProvider } from '../providers/rest-api/rest-api';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {HelpmetModalPage} from '../pages/helpmet-modal/helpmet-modal';
import {HelpMatePage} from '../pages/help-mate/help-mate';
import {SoftLoanPage} from '../pages/soft-loan/soft-loan';
import {PrePaymentLoanPage} from '../pages/pre-payment-loan/pre-payment-loan';
import {SoftloantabPage} from '../pages/softloantab/softloantab';
import { Deeplinks } from '@ionic-native/deeplinks';

@NgModule({
  declarations: [
    MyApp,
    DashboardPage,
    LoginPage,
    AppDashboardPage,
    WorkHistoryPage,
    ElBalancePage,
    LearningTabPage,
    MandatoryTrainigsPage,
    PastDueTrainingsPage,
    ActiveTrainigsPage,
    TabsPage,
    BasicDetailsPage,
    PersonalDetailsPage,
    HomePage,
    MangerViewPage,
    HelpMatePage,
    HelpmetModalPage,
    SoftLoanPage,
    PrePaymentLoanPage,
    SoftloantabPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ChartsModule,
    FormsModule,
    SuperTabsModule,
    IonicModule.forRoot(MyApp, { tabsPlacement: 'top', }),
    SuperTabsModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    DashboardPage,
    AppDashboardPage,
    WorkHistoryPage,
    ElBalancePage,
    LearningTabPage,
    MandatoryTrainigsPage,
    PastDueTrainingsPage,
    ActiveTrainigsPage,
    TabsPage,
    BasicDetailsPage,
    PersonalDetailsPage,
    HomePage,
    MangerViewPage,
    HelpMatePage,
    HelpmetModalPage,
    SoftLoanPage,
    PrePaymentLoanPage,
    SoftloantabPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Logger,
    RestApiProvider,
    InAppBrowser,
    Deeplinks,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
