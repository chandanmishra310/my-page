// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  templaterBaseUrl: 'http://192.168.1.230:8091/creator',
  baseUrl: 'https://genpacticsinstancedev01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/',
  storageServiceBaseUrl : 'http://192.168.1.230:9089/storageservice/v1/files/'
};

