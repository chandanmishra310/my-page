import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkHistoryPage } from './work-history';

@NgModule({
  declarations: [
    WorkHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkHistoryPage),
  ],
})
export class WorkHistoryPageModule {}
