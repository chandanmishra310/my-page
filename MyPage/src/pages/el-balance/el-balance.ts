import { IonicPage, NavController,ToastController, NavParams,AlertController,App} from 'ionic-angular';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { LoadingController } from 'ionic-angular';
import { RequestOptions } from '@angular/http';
import {TabsPage} from '../tabs/tabs';
import {LoginPage} from '../login/login';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Time } from '@angular/common';
import { Component,ViewChild} from '@angular/core';
import { DashboardPage } from '../dashboard/dashboard';




/**
 * Generated class for the ElBalancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-el-balance',
  templateUrl: 'el-balance.html',
})
export class ElBalancePage {
  @ViewChild('myNav') nav: NavController
  fName:string;
  lName:string;
  fullName:string;
  OHRID: string = localStorage.getItem("OHRID");
  static baseUrl: string = 'https://genpacticsinstancedev01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/';
  static getEmployeeUrl: string = ElBalancePage.baseUrl + 'REQUEST_LEAVE_GET_API/v01/EarnedLeaves?OHRID=';
  static userName = 'rgupta';
  static password = 'O@acle@123';

  maxtime = 20;
   timer: number;
   timeout = 0;

  name: string;
  leaveType: string;
  hrmName: string;
  hrmOHR: string;
  supervisorName: string;
  earnedLeaveBalance:string;
  encodedString: string;
  employeeDetailsResponse: EarnedLeavesDetail;
  constructor(private toastCtrl: ToastController,public alertCtrl:AlertController,public app: App,public navCtrl: NavController, public loadingCtrl: LoadingController, public navParams: NavParams, private http: HttpClient)
   {
    console.log('ohrid =====>');
    console.log(this.OHRID);
    console.log('navCtrl ==>');
    console.log(navCtrl);
    this.getAuthorizationKey();
    this.getEmployeeUrl();
  }
  ionViewDidLoad(){
    
    if(localStorage.getItem("fName") == null)
{
  this.fullName = "";
}else{
    this.fName = localStorage.getItem("fName");
    this.lName = localStorage.getItem("lName");
    this.fullName = this.fName + "," + this.lName;
  }
}

  goProfile()
{
  this.navCtrl.push(TabsPage);
}
  logout() {
    const confirm = this.alertCtrl.create({
      title: 'MYPAGE',
      message: 'Do you wish to Sign Out?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YES',
          handler: () => {
  
            const root = this.app.getRootNav();
            root.popToRoot();
              this.navCtrl.setRoot(LoginPage);
  
            this.navCtrl.popToRoot();
            console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');
  
          }
        }
      ]
    });
    confirm.present();
  }
  getAuthorizationKey() {
    const encodedString = btoa(ElBalancePage.userName + ":" + ElBalancePage.password);
    console.log('encodedString ==>');
    console.log(encodedString);
    this.encodedString = encodedString;
  }
  getEmployeeUrl() {
    let loader = this.loadingCtrl.create({
     // content: 'Getting latest entries...',
     spinner: 'hide',
     content: '<img src="assets/imgs/logogif.gif"/>',
    });

    // Show the popup
    this.StartTimer(loader);
    loader.present();

    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '850027873')
      .set('SecretKey', '29fb9858c6eed8ef');

    this.http.get(ElBalancePage.getEmployeeUrl + this.OHRID
      , { headers }).subscribe(
        res => {
          this.timeout = 1;
          loader.dismiss();
          console.log(res);
          // this.firstName = res.EmployeeDetail.FirstName;
          this.employeeDetailsResponse = <EarnedLeavesDetail>res;
          console.log('this.employeeDetailsResponse.EmployeeDetail.FirstName ==>');

          //console.log(this.employeeDetailsResponse.EarnedLeavesDetail.FirstName);

          //this.name = this.employeeDetailsResponse.EarnedLeavesDetail.EmployeeId;
          //this.leaveType = this.employeeDetailsResponse.EarnedLeavesDetail.FirstName;
          this.hrmName = this.employeeDetailsResponse.EarnedLeavesDetail.HRMName;
          this.hrmOHR = this.employeeDetailsResponse.EarnedLeavesDetail.HRMOHR;
          this.supervisorName = this.employeeDetailsResponse.EarnedLeavesDetail.SupervisorName;
          this.earnedLeaveBalance = this.employeeDetailsResponse.EarnedLeavesDetail.EarnedLeaveBalance;
          console.log('this.employeeDetailsResponse.EmployeeDetail.EarnedLeaveBalance ==>');
        },
        err => {
          console.error(err);
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
        },

        // () => {
        //   console.log('Request complete');
        //   loader.present().then(() => {

        //     loader.dismiss();
        //   });
        // }
      );
  }
  // ionViewDidLoad() {

  //   console.log('ionViewDidLoad ElBalancePage');
  // }
  StartTimer(loader) {
    this.timer = setTimeout(x => {
      if (this.maxtime <= 0) {
      }
      this.maxtime -= 1;
      if (this.maxtime > 0) {
        this.StartTimer(loader);
        console.log(this.maxtime);
      }
      else {
        if (this.timeout == 0) {
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();

          this.presentToast();

          //   let toast = this.toastCtrl.create({
          //     message: 'Toast Message',
          //     duration: 3000
          //   });
          //   toast.present();
          // }
        }
      }
    }, 1000);
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Internet Conection is lost ',
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
export interface EarnedLeavesDetail {
  EarnedLeavesDetail: {
    HRMName: string,
    HRMOHR: string,
    SupervisorName: string,
    EarnedLeaveBalance:string;
     }
}
