import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HelpmetModalPage } from './helpmet-modal';

@NgModule({
  declarations: [
    HelpmetModalPage,
  ],
  imports: [
    IonicPageModule.forChild(HelpmetModalPage),
  ],
})
export class HelpmetModalPageModule {}
