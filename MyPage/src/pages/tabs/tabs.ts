import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,App,AlertController,} from 'ionic-angular';
import {MangerViewPage} from '../manger-view/manger-view';
import {PersonalDetailsPage} from '../personal-details/personal-details';
import {BasicDetailsPage} from '../basic-details/basic-details';
import {HomePage} from '../home/home';
import { SuperTabsModule } from 'ionic2-super-tabs';
import {LoginPage} from '../login/login';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = BasicDetailsPage;
  tab2Root = PersonalDetailsPage;
  tab3Root = HomePage;
  tab4Root = MangerViewPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl:AlertController,public app: App) {
  }
  logout() {
    const confirm = this.alertCtrl.create({
      title: 'MYPAGE',
      message: 'Do you wish to Sign Out?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YES',
          handler: () => {
  
            const root = this.app.getRootNav();
            root.popToRoot();
              this.navCtrl.setRoot(LoginPage);
  
              this.navCtrl.popToRoot();
            console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');
  
          }
        }
      ]
    });

    confirm.present();
  }
}
