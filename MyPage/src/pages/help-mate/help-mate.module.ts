import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HelpMatePage } from './help-mate';

@NgModule({
  declarations: [
    HelpMatePage,
  ],
  imports: [
    IonicPageModule.forChild(HelpMatePage),
  ],
})
export class HelpMatePageModule {}
