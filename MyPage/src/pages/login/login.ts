import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DashboardPage } from '../dashboard/dashboard';
import {AppDashboardPage}  from '../app-dashboard/app-dashboard';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

  public user_email: any;
  public user_password: any;
  credentialsForm: FormGroup;
  showResultMessage = false;
  message: string;

  constructor(public navCtrl: NavController, private formBuilder: FormBuilder, public navParams: NavParams, public alertCtrl: AlertController, public modalCtrl: ModalController) {
    this.credentialsForm = this.formBuilder.group({
      user_email: ["", Validators.required],
      user_password: ["", Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');

    this.user_email = "";
    this.user_password = "";
  }

  ionViewWillEnter() {
    this.credentialsForm.reset();
     
  }
  // presentAlert() {
  //   let alert = this.alertCtrl.create({
  //     title: 'Information',
  //     subTitle: 'Please enter all information',
  //     buttons: ['Dismiss']
  //   });
  //   alert.present();
  // }
  logForm() {

  }
  verify() {
    this.showResultMessage = false;
    if (this.credentialsForm.valid) {
      this.user_email = this.credentialsForm.controls.user_email.value;
      this.user_password = this.credentialsForm.controls.user_password.value;
      // alert(tnhis.user_email);
      if ((this.user_email == "703182023")||(this.user_email == "303008989")||(this.user_email == "703198634")||(this.user_email == "703100186")||(this.user_email == "703030821")||(this.user_email == "703181797")||(this.user_email == "303007750")||(this.user_email == "703011504")||(this.user_email == "303027869")||(this.user_email == "302011033") || (this.user_email == "303003914") || (this.user_email == "703181403") || (this.user_email == "703007911") || (this.user_email == "303008989") || (this.user_email == "703010818")) {

        if (this.user_password != "welcome1") {
          this.showResultMessage = true;
          this.message = "Please enter valid password.";
        } else {
          let data = {
            loginInfo: this.user_email
          };
          localStorage.setItem("OHRID", this.user_email);
         /// this.navCtrl.setRoot(LoginPage);
         this.user_email = "";
         this.user_password = "";
          //this.navCtrl.push(AppDashboardPage, data);
          this.navCtrl.setRoot(AppDashboardPage, data);
        }
      } else {
        this.showResultMessage = true;
        this.message = "Please enter valid OHRID";
      }

    } else {
      Object.keys(this.credentialsForm.controls).forEach(field => { // {1}
        const control = this.credentialsForm.get(field);            // {2}
        control.markAsTouched({ onlySelf: true });       // {3}
      });
    }
  }

}