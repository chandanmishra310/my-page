import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
/**
 * Generated class for the SoftLoanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-soft-loan',
  templateUrl: 'soft-loan.html',
})
export class SoftLoanPage {

  selectloanamount: string = "";
  public user_email: any;
  public user_password: any;
  public loan_amount:any;
  public install_amount:any;
  credentialsForm: FormGroup;
  showResultMessage = false;
  message: string;
  paymentType:any;
  value:any;
  myDate:any;
  // constructor(public navCtrl: NavController, public navParams: NavParams) {
  // }
  constructor(public navCtrl: NavController, private formBuilder: FormBuilder, public navParams: NavParams, public alertCtrl: AlertController, public modalCtrl: ModalController) {
    this.credentialsForm = this.formBuilder.group({
      user_email: ["", Validators.required],
      user_password: ["", Validators.required],
      loan_amount:["",Validators.required],
      install_amount:["",Validators.required]
    });
   
  }
  onChange(paymentType) {
    if (this.paymentType == 'oneH') {
      this.value = 0;
      console.log("Neft", this.value + "");
    }
    else if (this.paymentType == 'twoH') {
      this.value = 1;
      console.log("Cheque", this.value + "");

    }
    else if (this.paymentType == 'threeH') {
      this.value = 2;
      console.log("Payroll", this.value + "");
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SoftLoanPage');
  }
  verify() {
    this.showResultMessage = false;
    if (this.credentialsForm.valid) {
      this.user_email = this.credentialsForm.controls.user_email.value;
      this.user_password = this.credentialsForm.controls.user_password.value;
      this.loan_amount = this.credentialsForm.controls.loan_amount.value;
      this.install_amount = this.credentialsForm.controls.install_amount.value;
      // alert(this.user_email);
      if (this.loan_amount.onChange().value== "" && this.install_amount.onChange().value== "" ) {
        this.showResultMessage = true;
        // this.message = "Please enter Amount";
      }
    } else {
      Object.keys(this.credentialsForm.controls).forEach(field => { // {1}
        const control = this.credentialsForm.get(field);            // {2}
        control.markAsTouched({ onlySelf: true });       // {3}
      });
    }

  }
}
