import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SoftLoanPage } from './soft-loan';

@NgModule({
  declarations: [
    SoftLoanPage,
  ],
  imports: [
    IonicPageModule.forChild(SoftLoanPage),
  ],
})
export class SoftLoanPageModule {}
