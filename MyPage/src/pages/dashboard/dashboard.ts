import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,App,Navbar } from 'ionic-angular';
import { NgModule } from '@angular/core';
import {WorkHistoryPage} from '../work-history/work-history';
import {ElBalancePage} from '../el-balance/el-balance';
import { TabsPage } from '../tabs/tabs' ;
import {LearningTabPage} from '../learning-tab/learning-tab' ;
import { HelpMatePage } from '../help-mate/help-mate';
import { LoginPage } from '../login/login';
import {SoftloantabPage} from '../softloantab/softloantab';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl:AlertController,public app: App) {
  }
goWork()
{
  this.navCtrl.push(WorkHistoryPage);
}
goLeave()
{
  this.navCtrl.push(ElBalancePage);
}
goLearning()
{
  this.navCtrl.push(LearningTabPage);
}
goProfile()
{
  this.navCtrl.push(TabsPage);
}
goHelp()
{
  this.navCtrl.push(HelpMatePage);
}
goSoftloan()
{
  this.navCtrl.push(SoftloantabPage);
}
logout() {
  const confirm = this.alertCtrl.create({
    title: 'MYPAGE',
    message: 'Do you wish to Sign Out?',
    buttons: [
      {
        text: 'NO',
        handler: () => {
          console.log('Disagree clicked');
        }
      },
      {
        text: 'YES',
        handler: () => {

          const root = this.app.getRootNav();
          root.popToRoot();
           this.navCtrl.setRoot(LoginPage);

            this.navCtrl.popToRoot();
          console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');

        }
      }
    ]
  });

  confirm.present();
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

}
