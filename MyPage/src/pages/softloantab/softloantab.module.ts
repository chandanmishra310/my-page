import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SoftloantabPage } from './softloantab';

@NgModule({
  declarations: [
    SoftloantabPage,
  ],
  imports: [
    IonicPageModule.forChild(SoftloantabPage),
  ]
})
export class SoftloantabPageModule {}
