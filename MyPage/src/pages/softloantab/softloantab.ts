import { Component } from '@angular/core';
import { IonicPage, NavController,App,AlertController } from 'ionic-angular';
import {SoftLoanPage} from '../soft-loan/soft-loan';
import {PrePaymentLoanPage} from '../pre-payment-loan/pre-payment-loan';
import {TabsPage} from '../tabs/tabs';
import {LoginPage} from '../login/login';
/**
 * Generated class for the SoftloantabPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-softloantab',
  templateUrl: 'softloantab.html'
})
export class SoftloantabPage {

  softLoanRoot = SoftLoanPage;
  prePaymentLoanRoot = PrePaymentLoanPage;


  constructor(public alertCtrl:AlertController,public app: App,public navCtrl: NavController) {}

  goProfile()
  {
    this.navCtrl.push(TabsPage);
  }
  logout() {
    const confirm = this.alertCtrl.create({
      title: 'MYPAGE',
      message: 'Do you wish to Sign Out?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YES',
          handler: () => {
  
            const root = this.app.getRootNav();
            root.popToRoot();
              this.navCtrl.setRoot(LoginPage);
  
              this.navCtrl.popToRoot();
            console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');
  
          }
        }
      ]
    });

    confirm.present();
  }
}
