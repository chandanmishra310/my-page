import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MandatoryTrainigsPage } from './mandatory-trainigs';

@NgModule({
  declarations: [
    MandatoryTrainigsPage,
  ],
  imports: [
    IonicPageModule.forChild(MandatoryTrainigsPage),
  ],
})
export class MandatoryTrainigsPageModule {}
