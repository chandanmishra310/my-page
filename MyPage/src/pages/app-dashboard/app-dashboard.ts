import { Component,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { IonicPage, NavController, NavParams ,App,AlertController,ViewController} from 'ionic-angular';
import {DashboardPage} from '../dashboard/dashboard';
import {LoginPage} from '../login/login';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Deeplinks } from '@ionic-native/deeplinks';


/**
 * Generated class for the AppDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-dashboard',
  templateUrl: 'app-dashboard.html',
})
export class AppDashboardPage {

   
  constructor(private deeplinks: Deeplinks,private  iab:InAppBrowser,public viewCtrl:ViewController,public navCtrl: NavController, public navParams: NavParams,public alertCtrl:AlertController,public app: App) {

  }
 

  openChatbot()
  {
  const browser = this.iab.create('https://hr.genpact.com/hr/peopleFirst/?env=cpub');
  browser.show()
  }
goNext(){
  this.navCtrl.push(DashboardPage);
}

goToBrowser()
{

// this.deeplinks.route({
//   '/spotify-callback/': AppDashboardPage
// }).subscribe((match) => {
//   // match.$route - the route we matched, which is the matched entry from the arguments to route()
//   // match.$args - the args passed in the link
//   // match.$link - the full link data
//   console.log('Successfully matched route', match);
// }, (nomatch) => {
//   // nomatch.$link - the full link data
//   console.error('Got a deeplink that didn\'t match');
//   console.log(nomatch);
// });

window.open("https://appurl.io/jk3qfm0j", '_system');

// window.open("mspbi://app/OpenReport?ReportObjectId=8eef1c25-0eb4-44e1-9949-bb828fa1395b", '_system');
 }

logout() {
  const confirm = this.alertCtrl.create({
    title: 'MYPAGE',
    message: 'Do you wish to Sign Out?',
    buttons: [
      {
        text: 'NO',
        handler: () => {
          console.log('Disagree clicked');
        }
      },
      {
        text: 'YES',
        handler: () => {
          const root = this.app.getRootNav();
          root.popToRoot();
            this.navCtrl.setRoot(LoginPage);

           this.navCtrl.popToRoot();
          console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');

        }
      }
    ]
  });

  confirm.present();
}
  ionViewDidLoad() {
   
    console.log('ionViewDidLoad AppDashboardPage');
  }

}