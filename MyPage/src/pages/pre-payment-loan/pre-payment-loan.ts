import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DashboardPage } from '../dashboard/dashboard';
/**
 * Generated class for the PrePaymentLoanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pre-payment-loan',
  templateUrl: 'pre-payment-loan.html',
})
export class PrePaymentLoanPage {

  selectloanamount: string = "";
  public user_amount: any;
  public user_email: any;
  public user_password: any;
  public user_payment: any;
  public user_transaction: any;
  public user_date:any;
  credentialsForm: FormGroup;
  showResultMessage = false;
  message: string;
  paymentType: any;
  value: any;


  constructor(public navCtrl: NavController, private formBuilder: FormBuilder, public navParams: NavParams, public alertCtrl: AlertController, public modalCtrl: ModalController) {

    this.credentialsForm = this.formBuilder.group({
      user_email: ["", Validators.required],
      user_password: ["", Validators.required],
      user_payment: ["", Validators.required],
      user_amount: ["", Validators.required],
      user_transaction: ["", Validators.required],
      user_date:["",Validators.required]
    });

  }

  onChange(paymentType) {
    if (this.paymentType == 'oneH') {
      this.value = 0;
      console.log("Neft", this.value + "");
    }
    else if (this.paymentType == 'twoH') {
      this.value = 1;
      console.log("Cheque", this.value + "");

    }
    else if (this.paymentType == 'threeH') {
      this.value = 2;
      console.log("Payroll", this.value + "");
    }
  }
  verify() {
    this.showResultMessage = false;
    if (this.credentialsForm.valid) {
      this.user_amount = this.credentialsForm.controls.user_amount.value;
      this.user_password = this.credentialsForm.controls.user_password.value;
      this.user_payment = this.credentialsForm.controls.user_payment.value;
      this.user_transaction = this.credentialsForm.controls.user_transaction.value;
      // alert(this.user_email);
      if (this.user_payment.onChange().value == "") {
        this.showResultMessage = true;
        // this.message = "Please enter Amount";
      }
      if (this.user_transaction == "") {
        this.showResultMessage = true;
        // this.message = "Please enter Amount";
      }
      if (this.user_date  == "") {
        this.showResultMessage = true;
        // this.message = "Please enter Amount";
      }
      // else
      //   this.user_amount = "";
      // this.user_password = "";
      // this.navCtrl.push(DashboardPage);

    } else {
      Object.keys(this.credentialsForm.controls).forEach(field => { // {1}
        const control = this.credentialsForm.get(field);            // {2}
        control.markAsTouched({ onlySelf: true });       // {3}
      });
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PrePaymentLoanPage');
    // this.value = 0;
  }
}
