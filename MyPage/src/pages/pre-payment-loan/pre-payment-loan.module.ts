import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrePaymentLoanPage } from './pre-payment-loan';

@NgModule({
  declarations: [
    PrePaymentLoanPage,
  ],
  imports: [
    IonicPageModule.forChild(PrePaymentLoanPage),
  ],
})
export class PrePaymentLoanPageModule {}
