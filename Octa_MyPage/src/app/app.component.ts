import { Component ,ViewChild} from '@angular/core';''
import {AppDashboardPage} from '../pages/app-dashboard/app-dashboard';
import {LoginPage} from '../pages/login/login';
import {OktaLoginPage} from '../pages/okta-login/okta-login';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { ToastController, NavController, AlertController, NavPop, App, NavParams,LoadingController } from 'ionic-angular';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any = AppDashboardPage;
  
    constructor(oauthService: OAuthService) {
 
    if (oauthService.hasValidIdToken()) {
      this.rootPage = AppDashboardPage;
    } else {
      this.rootPage = LoginPage;
    }
    
  }
 
}
