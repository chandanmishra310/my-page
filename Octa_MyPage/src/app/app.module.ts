import { IonicStorageModule } from '@ionic/storage';
import { OAuthModule } from 'angular-oauth2-oidc';
import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginPage } from '../pages/login/login';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { AppDashboardPage } from '../pages/app-dashboard/app-dashboard';
import { WorkHistoryPage } from '../pages/work-history/work-history';
import { ElBalancePage } from '../pages/el-balance/el-balance';
import { LearningTabPage } from '../pages/learning-tab/learning-tab';
import { MandatoryTrainigsPage } from '../pages/mandatory-trainigs/mandatory-trainigs';
import { PastDueTrainingsPage } from '../pages/past-due-trainings/past-due-trainings';
import { ActiveTrainigsPage } from '../pages/active-trainigs/active-trainigs';
import { ChartsModule } from 'ng2-charts';
import { TabsPage } from '../pages/tabs/tabs';
import { BasicDetailsPage } from '../pages/basic-details/basic-details';
import { PersonalDetailsPage } from '../pages/personal-details/personal-details';
import { HomePage } from '../pages/home/home';
import { MangerViewPage } from '../pages/manger-view/manger-view';
import { HelpMatePage } from '../pages/help-mate/help-mate';
import { Navbar } from 'ionic-angular';
import { SuperTabsModule } from 'ionic2-super-tabs';
import {OktaLoginPage } from '../pages/okta-login/okta-login';
import {HelpmetModalPage} from '../pages/helpmet-modal/helpmet-modal';
import { InAppBrowser } from '@ionic-native/in-app-browser';


@NgModule({
  declarations: [
    MyApp,
    DashboardPage,
    LoginPage,
    AppDashboardPage,
    WorkHistoryPage,
    ElBalancePage,
    LearningTabPage,
    MandatoryTrainigsPage,
    PastDueTrainingsPage,
    ActiveTrainigsPage,
    TabsPage,
    BasicDetailsPage,
    PersonalDetailsPage,
    HomePage,
    MangerViewPage,
    HelpMatePage,
    OktaLoginPage,
    HelpmetModalPage,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ChartsModule,
    FormsModule,
    SuperTabsModule,
    IonicModule.forRoot(MyApp, {
      tabsPlacement: 'top',
      preloadModules: true
    }),
    IonicStorageModule.forRoot(),
    SuperTabsModule.forRoot(),
    OAuthModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DashboardPage,
    LoginPage,
    AppDashboardPage,
    WorkHistoryPage,
    ElBalancePage,
    LearningTabPage,
    MandatoryTrainigsPage,
    PastDueTrainingsPage,
    ActiveTrainigsPage,
    TabsPage,
    BasicDetailsPage,
    PersonalDetailsPage,
    HomePage,
    MangerViewPage,
    HelpMatePage,
    OktaLoginPage,
    HelpmetModalPage,
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
