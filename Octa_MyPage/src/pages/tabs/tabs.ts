import { Component } from '@angular/core';
import { IonicPage,ToastController , NavController, NavParams, App, AlertController, } from 'ionic-angular';
import { MangerViewPage } from '../manger-view/manger-view';
import { PersonalDetailsPage } from '../personal-details/personal-details';
import { BasicDetailsPage } from '../basic-details/basic-details';
import { HomePage } from '../home/home';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { LoginPage } from '../login/login';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { DashboardPage } from '../dashboard/dashboard';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = BasicDetailsPage;
  tab2Root = PersonalDetailsPage;
  tab3Root = HomePage;
  tab4Root = MangerViewPage;

  constructor(private toastCtrl: ToastController,private oauthService: OAuthService, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public app: App) {

    let timeoutString : String = localStorage.getItem("timeout")
    console.log(timeoutString + "timeout string")
    // if (timeoutString === "1") {
      // const confirm = this.alertCtrl.create({
      //   title: 'MYPAGE',
      //   message: 'Internet Connection is lost.',
      //   buttons: [
      //     {
      //       text: 'OK',
      //       handler: () => {
      //         // this.navCtrl.pop()
      //         // console.log("timeout ho gaya hai bhai.")
      //         // localStorage.removeItem("timeout")
      //         // localStorage.setItem("timeout","2")
      //         const root = this.app.getRootNav();
      //         root.popToRoot();
      //         this.navCtrl.setRoot(DashboardPage);
      //         this.navCtrl.popToRoot();
      //       }
      //     }
      //   ]
      // });
      // confirm.present();

      // const root = this.app.getRootNav();
      // root.popToRoot();
      // this.navCtrl.setRoot(DashboardPage);
      // this.navCtrl.popToRoot();
    // }

    //localStorage.removeItem("timeout")
   //
    localStorage.removeItem("timeout")
  }

  logout() {
    const confirm = this.alertCtrl.create({
      title: 'MYPAGE',
      message: 'Do you wish to Sign Out?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YES',
          handler: () => {
            this.oauthService.logOut();
            // const root = this.app.getRootNav();
            // root.popToRoot();
            // this.navCtrl.setRoot(LoginPage);

            // this.navCtrl.popToRoot();
            // console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');

          }
        }
      ]
    });

    confirm.present();
  }

 ionViewDidLoad()
{
localStorage.removeItem("timeout")

}

}
