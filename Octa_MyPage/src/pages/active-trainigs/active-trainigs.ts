import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { MyApp } from '../../app/app.component';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';
import 'rxjs/add/observable/of';
import 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { LoadingController } from 'ionic-angular';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { DashboardPage } from '../dashboard/dashboard';

/**
 * Generated class for the ActiveTrainigsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-active-trainigs',
  templateUrl: 'active-trainigs.html',
})
export class ActiveTrainigsPage {
  myTrainingsCollection: MyTrainingsCollection[] = [];
  myTrainings: MyTrainings;
  navParams: NavParams;
  maxtime = 20;
  timer: number;
  timeout = 0;
  //OHRID = '302011033';
  //OHRID: string = localStorage.getItem("OHRID")
  OHRID:string = localStorage.getItem("emailname");
  encodedString: string;


  static baseUrl: string = 'https://genpacticsinstancedev01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/';
  static myTrainingUrl: string = ActiveTrainigsPage.baseUrl + 'ILEARN_MYTRAINING_API/v01/myTrainings?OHRID=';
  static userName = 'rgupta';
  static password = 'O@acle@123';


  constructor(public navCtrl: NavController, private alertCtrl: AlertController, public app: App, private http: HttpClient, public loadingCtrl: LoadingController, navParams: NavParams) {

    // this.myTrainingsCollection = navParams.get('id');
    // console.log('asdjlhasjdahsdgahsgdasdfg'+this.myTrainingsCollection)


    //this.getAuthorizationKey();
    //this.getEmployeeUrl();
  }
  ionViewDidLoad() {

    // this.myTrainingsCollection = this.navParams.get('id');
    // console.log('data '+this.myTrainingsCollection)
    // console.log('ionViewDidLoad MandatoryTrainigsPage');
  }

  getAuthorizationKey() {
    const encodedString = btoa(ActiveTrainigsPage.userName + ":" + ActiveTrainigsPage.password);
    console.log('encodedString ==>');
    console.log(encodedString);
    this.encodedString = encodedString;
  }

  getEmployeeUrl() {
    let loader = this.loadingCtrl.create({
      spinner: 'hide',
      content: '<img src="assets/imgs/logogif.gif"/>',
    });

    //Show the popup
    this.StartTimer(loader);
    loader.present();

    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '850027873')
      .set('SecretKey', '29fb9858c6eed8ef');
    let result: string[];
    let hh: string;
    let mm: string;
    let Url: string;
    let newUrl: string;

    this.http.get(ActiveTrainigsPage.myTrainingUrl + this.OHRID
      , { headers }).subscribe(
        res => {
          this.timeout = 1;
          loader.dismiss();
          console.log(res);
          // this.firstName = res.EmployeeDetail.FirstName;
          this.myTrainings = <MyTrainings>res;
          console.log('this.myTrainings.MyTrainingsCollection ==>');

          console.log(this.myTrainings);
          this.myTrainingsCollection = this.myTrainings.MyTrainings.MyTrainingsCollection.map(function (value, index, array) {

            let genp: string = "https://genpact.csod.com/lms/scorm/clientLMS/ScormFrames.aspx?";
            //result = value.TotalTrainingHours.split(":");
            Url = value.TrainingURL;
            newUrl = Url + genp;
            //             hh = result[0].slice(2, result[0].length);
            //             mm = result[1];
            //             value.TotalTrainingHours = hh + ':' + mm;

            //             result = value.TotalTime.split(":");
            //             hh = result[0].slice(2, result[0].length);
            //             mm = result[1];
            //             value.TotalTime = hh + ':' + mm;

            return value;
          }, this.myTrainingsCollection);

        },
        err => {
          console.error(err);
        },

        () => {
          console.log('Request complete');
          loader.present().then(() => {

            loader.dismiss();
          });
        }
      );
  }
  StartTimer(loader) {
    this.timer = setTimeout(x => {
      if (this.maxtime <= 0) {
      }
      this.maxtime -= 1;
      if (this.maxtime > 0) {
        this.StartTimer(loader);
        console.log(this.maxtime);
      }
      else {
        if (this.timeout == 0) {
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
          // localStorage.setItem("1","timeout")
        }
      }
    }, 1000);
  }
  ionViewWillEnter() {
    console.log('ionViewDidLoad PersonalDetailsPage');
    this.getAuthorizationKey();
    this.getEmployeeUrl();
  }
}

export interface MyTrainingsCollection {
  OHRID: string,
  TrainingID: string,
  TrainingTitle: string,
  TrainingCompletionDate: string,
  Status: string,
  DueDate: string,
  TotalTime: string,
  TotalTrainingHours: string,
  TrainingScore: string,
  Provider: string,
  TrainingURL: string
}

export interface MyTrainings {
  MyTrainings: {
    MyTrainingsCollection: MyTrainingsCollection[]
  }
}
