import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActiveTrainigsPage } from './active-trainigs';

@NgModule({
  declarations: [
    ActiveTrainigsPage,
  ],
  imports: [
    IonicPageModule.forChild(ActiveTrainigsPage),
  ],
})
export class ActiveTrainigsPageModule {}
