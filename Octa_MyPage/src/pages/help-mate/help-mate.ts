import { Component, ComponentFactoryResolver } from '@angular/core';
import { IonicPage, ModalController, App, NavController, AlertController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { RequestOptions, Headers } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { DashboardPage } from '../dashboard/dashboard';
import { HelpmetModalPage } from '../helpmet-modal/helpmet-modal';

@IonicPage()
@Component({
  selector: 'page-help-mate',
  templateUrl: 'help-mate.html',
})
export class HelpMatePage {
  maxtime = 20;
  timer: number;
  timeout = 0;
  encodedString: string;
  OHRID: string = '703032500';
  //OHRID:string = localStorage.getItem("emailname");
  //OHRID:string;

  Request_Number__c: any;
  signature_InstanceID: any;
  approval_Status: any;
  approver_OHR: any;
  justification: any;
  request_ID: any;

  //OHRID: string = localStorage.getItem("OHRID");
  myTrainingsCollection: Helpmate[] = [];
  myTrainings: HelpmateGetCollection;
  //i:number;
  employeeDetailsResponse: EmployeeDetails;
  employeeId: string;
  firstName: string = "";
  lastName: string = "";
  originalDateOfHire: string;
  bandCode: string;
  designation: string;
  department: string;
  supervisorName: string;
  locationCode: string;
  locationId: string;
  gender: string;
  dob: string;
  email: string;
  address1: string;
  address2: string;
  postalCode: string;
  region: string;
  assAttribute21: string;
  assAttribute22: string;
  country: string;
  pfNumberFirst: string;
  pfNumberSecond: string;
  uanNumber: string;
  pfNumberFs: string;
  pfNumberSc: string;
  leCode: string;
  show: boolean = true;

  static baseUrl_help: string = 'https://genpacticsinstancedev01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/';
  static gethelpmodel: string = HelpMatePage.baseUrl_help + 'EMPLOYEE_DETAILS_INTEGRATION/v01/getEmployeeData?OHRID=';


  static baseUrl: string = 'https://genpacticsinstancetest01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/';
  static getEmployeeUrl: string = HelpMatePage.baseUrl + 'HELPMATE_GET_API/v01/getTickets?OHRID=';
  //static myTrainingUrl: string = WorkHistoryPage.baseUrl + 'ILEARN_MYTRAINING_API/v01/myTrainings?OHRID=';

  static userName = 'rgupta';
  static password = 'O@acle@123';
  constructor(public modalCtrl: ModalController, private oauthService: OAuthService, public app: App, public navCtrl: NavController, public navParams: NavParams, private http: HttpClient, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public toastCtrl: ToastController) {
    //this.i=0;
    //this. OHRID = localStorage.getItem("OHRID");
    console.log('ohrid =====>');
    console.log(this.OHRID);
    console.log('navCtrl ==>');
    console.log(navCtrl);
    this.getAuthorizationKey();
    this.getEmployeeUrl();
  }
  openModal(i: number) {
    let myModal = this.modalCtrl.create(HelpmetModalPage);

    localStorage.setItem("reqest_num", this.myTrainingsCollection[i].Request_Number__c);
    localStorage.setItem("reqest_name", this.myTrainingsCollection[i].Full_Name__c);
    localStorage.setItem("reqest_ohr", this.myTrainingsCollection[i].Requested_For_Login_ID__c);
    localStorage.setItem("base_location", this.myTrainingsCollection[i].Site__c);
    localStorage.setItem("details", this.myTrainingsCollection[i].Details__c);

    myModal.present();
  }
  logout() {
    const confirm = this.alertCtrl.create({
      title: 'MYPAGE',
      message: 'Do you wish to Sign Out?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YES',
          handler: () => {
            this.oauthService.logOut();
            // const root = this.app.getRootNav();
            // root.popToRoot();
            //   this.navCtrl.setRoot(LoginPage);

            // this.navCtrl.popToRoot();
            // console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');

          }
        }
      ]
    });
    confirm.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad BasicDetailsPage');
  }
  ionViewWillEnter()
  {
    this.getHelpmetModal();
  }
  goProfile() {
    this.navCtrl.push(TabsPage);
  }
  getAuthorizationKey() {
    const encodedString = btoa(HelpMatePage.userName + ":" + HelpMatePage.password);
    console.log('encodedString ==>');
    console.log(encodedString);
    this.encodedString = encodedString;
  }
  getHelpmetModal() {

    //localStorage.setItem("2", "timeout")
    let loader = this.loadingCtrl.create({
      cssClass: 'myClass',
      spinner: 'hide',
      content: '<img src="assets/imgs/logogif.gif"/>',
    });

    this.StartTimer(loader);
    // Show the popup
    loader.present();
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '850027873')
      .set('SecretKey', '29fb9858c6eed8ef');

    this.http.get(HelpMatePage.gethelpmodel + this.OHRID
      , { headers }).subscribe(
        res => {
          console.log(res);
          this.timeout = 1;
          loader.dismiss();
          this.employeeDetailsResponse = <EmployeeDetails>res;

          this.bandCode = this.employeeDetailsResponse.EmployeeDetail.BandCode;
          this.assAttribute21 = this.employeeDetailsResponse.EmployeeDetail.AssAttribute21;
          this.assAttribute22 = this.employeeDetailsResponse.EmployeeDetail.AssAttribute22;
          this.leCode = this.employeeDetailsResponse.EmployeeDetail.LeCode;
          this.supervisorName = this.employeeDetailsResponse.EmployeeDetail.SupervisorName;

          localStorage.setItem("bandCode", this.bandCode);
          localStorage.setItem("assAttribute21", this.assAttribute21);
          localStorage.setItem("assAttribute22", this.assAttribute22);
          localStorage.setItem("leCode", this.leCode);
          localStorage.setItem("supervisorName", this.supervisorName);

        },
        err => {
          console.error(err);
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
        },
    );
  }
  getEmployeeUrl() {

    let loader = this.loadingCtrl.create({
      spinner: 'hide',
      content: '<img src="assets/imgs/logogif.gif"/>',
    });

    // Show the popup
    this.StartTimer(loader);
    loader.present();

    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '800083326')
      .set('SecretKey', 'a8658f0d315d2459');
    let result: string[];
    let hh: string;
    let mm: string;
    let value1: any;

    this.http.get(HelpMatePage.getEmployeeUrl + this.OHRID
      , { headers }).subscribe(
        res => {
          this.timeout = 1;
          loader.dismiss();
          console.log("Response-----" + JSON.stringify(res));
          // this.firstName = res.EmployeeDetail.FirstName;
          this.myTrainings = <HelpmateGetCollection>res;
          // console.log('this.myTrainings.HelpmateGetCollection ==>' + JSON.stringify(this.myTrainings));

          //console.log(myTrainingsCollection.TitleFromSRD__c);

          this.myTrainingsCollection = this.myTrainings.HelpmateGetCollection.Helpmate.map(function (value, index, array) {

            localStorage.setItem("reqest_num", value.Request_Number__c);
            localStorage.setItem("reqest_name", value.Full_Name__c);
            localStorage.setItem("reqest_ohr", value.Requested_For_Login_ID__c);
            localStorage.setItem("base_location", value.Site__c);

            return value;
          }, this.myTrainingsCollection);

        },
        err => {
          //console.error(err);
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
        },

        // () => {
        //   // console.log('Request complete');

        //   //this.postDataMethod()
        
        //   loader.present().then(() => {

        //     loader.dismiss();
        //   });
        // }
      );
  }

  postDataMethod(ticketStatus: string) {


    let loader = this.loadingCtrl.create({
      spinner: 'hide',
      content: '<img src="assets/imgs/logogif.gif"/>',
    });

    // Show the popup
    this.StartTimer(loader);
    loader.present();

    const headers = new HttpHeaders()

      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '800083326')
      .set('SecretKey', 'a8658f0d315d2459')
      .set('Content-Type', 'application/json');

    let result: string[];
    let hh: string;
    let mm: string;
    let value1: any

    let body = {
      Signature_InstanceID: this.signature_InstanceID,
      Approval_Status: this.approval_Status,
      Approver_OHR: this.approver_OHR,
      Justification: this.justification

    };

    console.log('request' + body);
    console.log('Header' + headers);

    this.http.post('https://genpacticsinstancetest01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/HELPMATE_UPDATE_API/v01/updateTicket', body
      , { headers }).subscribe(
        res => {
          this.timeout = 1;
          loader.dismiss();
          let message: string = 'Ticket ' + ticketStatus + ' for REQ' + this.request_ID;
          this.showAlert(message);


          //alert('Ticket '+ ticketStatus+ ' for REQ'+this.request_ID);
        },
        err => {
          console.log(err);
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
        },

        () => {
          loader.present().then(() => {
            loader.dismiss();
          });
        }
      );
  }

  goApprove(i: number, status: any) {

    this.signature_InstanceID = this.myTrainingsCollection[i].Signature_Instance_ID__c;
    this.approval_Status = this.myTrainingsCollection[i].Approval_Status;
    this.approver_OHR = this.myTrainingsCollection[i].Approvers;
    this.justification = status;
    this.request_ID = this.myTrainingsCollection[i].Request_ID;


    this.myTrainingsCollection.splice(i, 1);

    if (status === "Approving Ticket") {
      let ticketStatus: string = 'approved'
      this.postDataMethod(ticketStatus)
    } else {
      let ticketStatus: string = 'rejected'
      this.postDataMethod(ticketStatus)
    }
  }

  showAlert(message: string) {

    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  StartTimer(loader) {
    this.timer = setTimeout(x => {
      if (this.maxtime <= 0) {
      }
      this.maxtime -= 1;
      if (this.maxtime > 0) {
        this.StartTimer(loader);
        console.log(this.maxtime);
      }
      else {
        if (this.timeout == 0) {
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
          // localStorage.setItem("1","timeout")
        }
      }
    }, 1000);
  }

}


//   showAlert(message:string) {
//     const alert = this.alertCtrl.create({
//       title: message,
//       subTitle: '',
//       buttons: ['OK']
//     });
//     alert.present();
//   }

// }

export interface Helpmate {
  TitleFromSRD__c: string,
  Request_Number__c: string,
  Submit_Date__c: string,
  Signature_Instance_ID__c: string,
  Approval_Status: string,
  Approvers: string,
  Request_ID: string,
  Full_Name__c: string;
  Requested_For_Login_ID__c: string;
  Site__c: string;
  Details__c: string;
}
export interface HelpmateGetCollection {
  HelpmateGetCollection: {
    Helpmate: Helpmate[]
  }
}
export interface EmployeeDetails {
  EmployeeDetail: {
    EmployeeId: string,
    IlearnActive: string,
    Absent: string,
    AllowReconcile: string,
    Title: string,
    FirstName: string,
    MiddleName: string,
    LastName: string,
    Suffix: string,
    EmailAdd: string,
    PhoneWork: string,
    PhoneHome: string,
    PhoneNumber: string,
    PhoneFax: string,
    Country: string,
    Line1: string,
    Line2: string,
    Region2: string,
    PostalCode: string,
    PoleCode: string,
    LocationId: string,
    JobId: string,
    AssAttribute25: string,
    GradeId: string,
    DateStart: string,
    OriginalDateOfHire: string,
    OrganizationApprovals: string,
    IlearnApprover: string,
    ManagerEmployeeNum: string,
    Sex: string,
    GetLookupMeaning: string,
    PersonTypeId: string,
    BusinessGroupId: string,
    AssAttribute21: string,
    AssAttribute22: string,
    AssAttribute24: string,
    OrganizationId: string,
    ServiceLine: string,
    AccountCode: string,
    ProductId: string,
    ProductFamilyId: string,
    InternalIndustryViewL1: string,
    FpnaOhr: string,
    AvpOhr: string,
    UnigenUser: string,
    HireType: string,
    ServiceAgreementType: string,
    GdcFlag: string,
    BizSeg: string,
    BusinessSegments: string,
    HrManager: string,
    MatrixManager: string,
    TrainingManager: string,
    TrainingLeader: string,
    ProgramInitiativeManager1: string,
    Contractor: string,
    LoginId: string,
    LocaleCode: string,
    CountryCode: string,
    LedgerCode: string,
    ReimbCurrCode: string,
    CashAdvAcc: string,
    OrgUnit1: string,
    OrgUnit2: string,
    OrgUnit3: string,
    Custom1: string,
    Custom9: string,
    Custom10: string,
    Custom11: string,
    Custom12: string,
    Custom13: string,
    Custom14: string,
    Custom15: string,
    Custom16: string,
    Custom21: string,
    Traveler: string,
    BiManager: string,
    TravelReqUser: string,
    FutureUse2: string,
    CliqbookUser: string,
    FutureUse3: string,
    FutureUse4: string,
    FutureUse5: string,
    Designation: string,
    ErmId: string,
    BandCode: string,
    LeCode: string,
    PpcProcessName: string,
    Qualification: string,
    EmployeeStatus: string,
    SupervisorName: string,
    Pole: string,
    Sdo: string,
    LocationCode: string,
    DateExit: string,
    SupervisorEmail: string,
    EmpCode: string,
    EfmfmProcessId: string,
    Loc: string,
    EmpEmployment: string,
    EfmfmCity: string,
    Department: string,
    UserPersonType: string,
    CountryName: string,
    DataPort: string,
    VoicePort: string,
    EmergencyContactName: string,
    EmergencyContactNumber: string,
    PfAccountNumber: string,
    UniversalAccountNumber: string,
    AddressWheelsSite: string,
    SecondaryEmailId: string,
    Domain: string,
    WfhStatus: string,
    LastUpdateDate: string,
    DOB: string,
    HrManagerNumber: string,
    LastIncrementDate: string,
    NextIncrementDueDate: string,
    CurrDate: string,
    Skills: string,
    HRM_Name: string,
    ADHAAR: string,
    MARITAL_STATUS: string,
    LE_NAME: string
  }
}