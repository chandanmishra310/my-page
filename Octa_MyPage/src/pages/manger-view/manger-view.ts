import { Component } from '@angular/core';
import { NavController, AlertController, NavPop,App,NavParams } from 'ionic-angular';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { RequestOptions } from '@angular/http';
import { MyApp } from '../../app/app.component';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';
import 'rxjs/add/observable/of';
import 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { LoadingController } from 'ionic-angular';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { LoginPage } from '../login/login';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
/**
 * Generated class for the MangerViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-manger-view',
  templateUrl: 'manger-view.html',
})
export class MangerViewPage {

  //OHRID: string = localStorage.getItem("OHRID");
  OHRID:string = localStorage.getItem("emailname");
  //OHRID: string ='302011033';

  static baseUrl: string = 'https://genpacticsinstancedev01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/';
  static getEmployeeUrl: string = MangerViewPage.baseUrl + 'EMPLOYEE_DETAILS_INTEGRATION/v01/getEmployeeData?OHRID=';
  static myTrainingUrl: string = MangerViewPage.baseUrl + 'ILEARN_MYTRAINING_API/v01/myTrainings?OHRID=';
  static userName = 'rgupta';
  static password = 'O@acle@123';

  employeeDetailsResponse: EmployeeDetails;
  employeeId: string;
  firstName: string;
  lastName: string;
  originalDateOfHire:string;
  bandCode:string;
  designation:string;
  department: string;
  supervisorName:string;
  locationCode:string;
  locationId:string;
  gender: string;
  dob: string;
  email: string;
  address1: string;
  address2: string;
  postalCode: string;
  
  region: string;
  country: string;
  encodedString: string;
  constructor(private oauthService: OAuthService,public alertCtrl:AlertController,public navCtrl: NavController, private http: HttpClient,public app:App,public loadingCtrl: LoadingController, public navParams: NavParams) {
    console.log('ohrid =====>');
    console.log(this.OHRID);
    console.log('navCtrl ==>');
    console.log(navCtrl);
    //this.getAuthorizationKey();
    //this.getEmployeeUrl();
  }
  logout() {
    const confirm = this.alertCtrl.create({
      title: 'MYPAGE',
      message: 'Do you wish to Sign Out?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YES',
          handler: () => {
            this.oauthService.logOut();
            const root = this.app.getRootNav();
            root.popToRoot();
            //  this.navCtrl.setRoot(LoginPage);
  
            //  this.navCtrl.popToRoot();
            console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');
  
          }
        }
      ]
    });
  
    confirm.present();
  }
  getAuthorizationKey() {
    const encodedString = btoa(MangerViewPage.userName + ":" + MangerViewPage.password);
    console.log('encodedString ==>');
    console.log(encodedString);
    this.encodedString = encodedString;
  }
  getEmployeeUrl() {
    let loader = this.loadingCtrl.create({
      //content: 'Getting latest entries...',
      spinner: 'hide',
      content: '<img src="assets/imgs/logogif.gif"/>',
    });

    // Show the popup
    loader.present();

    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '850027873')
      .set('SecretKey', '29fb9858c6eed8ef');

    this.http.get(MangerViewPage.getEmployeeUrl+this.OHRID
      , { headers }).subscribe(
        res => {
          console.log(res);
         // this.firstName = res.EmployeeDetail.FirstName;
          this.employeeDetailsResponse = <EmployeeDetails>res;
          console.log('this.employeeDetailsResponse.EmployeeDetail.FirstName ==>');

          console.log(this.employeeDetailsResponse.EmployeeDetail.FirstName);

          this.employeeId = this.employeeDetailsResponse.EmployeeDetail.EmployeeId;
          this.firstName = this.employeeDetailsResponse.EmployeeDetail.FirstName;
          this.lastName = this.employeeDetailsResponse.EmployeeDetail.LastName;
          this.originalDateOfHire = this.employeeDetailsResponse.EmployeeDetail.OriginalDateOfHire;
          this.bandCode = this.employeeDetailsResponse.EmployeeDetail.BandCode;
          this.designation = this.employeeDetailsResponse.EmployeeDetail.Designation;
          this.supervisorName = this.employeeDetailsResponse.EmployeeDetail.SupervisorName;
          this.locationCode = this.employeeDetailsResponse.EmployeeDetail.LocationCode;
          this.locationId = this.employeeDetailsResponse.EmployeeDetail.LocationId;
          this.gender = this.employeeDetailsResponse.EmployeeDetail.Sex;
          this.dob = this.employeeDetailsResponse.EmployeeDetail.DOB;
          this.email = this.employeeDetailsResponse.EmployeeDetail.EmailAdd;
          this.address1 = this.employeeDetailsResponse.EmployeeDetail.Line1;
          this.address2 = this.employeeDetailsResponse.EmployeeDetail.Line2;
          this.postalCode = this.employeeDetailsResponse.EmployeeDetail.PostalCode;
          this.department = this.employeeDetailsResponse.EmployeeDetail.Department;
          this.region = this.employeeDetailsResponse.EmployeeDetail.Region2;
          this.country = this.employeeDetailsResponse.EmployeeDetail.Country;
        },
        err => {
          console.error(err);
        },

        () => {
          console.log('Request complete');
          loader.present().then(() => {

            loader.dismiss();
          });
        }
      );
  }
    ionViewDidLoad() {
    console.log('ionViewDidLoad MangerViewPage');
  }
}
export interface EmployeeDetails {
  EmployeeDetail: {
    EmployeeId: string,
    IlearnActive: string,
    Absent: string,
    AllowReconcile: string,
    Title: string,
    FirstName: string,
    MiddleName: string,
    LastName: string,
    Suffix: string,
    EmailAdd: string,
    PhoneWork: string,
    PhoneHome: string,
    PhoneNumber: string,
    PhoneFax: string,
    Country: string,
    Line1: string,
    Line2: string,
    Region2: string,
    PostalCode: string,
    PoleCode: string,
    LocationId: string,
    JobId: string,
    AssAttribute25: string,
    GradeId: string,
    DateStart: string,
    OriginalDateOfHire: string,
    OrganizationApprovals: string,
    IlearnApprover: string,
    ManagerEmployeeNum: string,
    Sex: string,
    GetLookupMeaning: string,
    PersonTypeId: string,
    BusinessGroupId: string,
    AssAttribute21: string,
    AssAttribute22: string,
    AssAttribute24: string,
    OrganizationId: string,
    ServiceLine: string,
    AccountCode: string,
    ProductId: string,
    ProductFamilyId: string,
    InternalIndustryViewL1: string,
    FpnaOhr: string,
    AvpOhr: string,
    UnigenUser: string,
    HireType: string,
    ServiceAgreementType: string,
    GdcFlag: string,
    BizSeg: string,
    BusinessSegments: string,
    HrManager: string,
    MatrixManager: string,
    TrainingManager: string,
    TrainingLeader: string,
    ProgramInitiativeManager1: string,
    Contractor: string,
    LoginId: string,
    LocaleCode: string,
    CountryCode: string,
    LedgerCode: string,
    ReimbCurrCode: string,
    CashAdvAcc: string,
    OrgUnit1: string,
    OrgUnit2: string,
    OrgUnit3: string,
    Custom1: string,
    Custom9: string,
    Custom10: string,
    Custom11: string,
    Custom12: string,
    Custom13: string,
    Custom14: string,
    Custom15: string,
    Custom16: string,
    Custom21: string,
    Traveler: string,
    BiManager: string,
    TravelReqUser: string,
    FutureUse2: string,
    CliqbookUser: string,
    FutureUse3: string,
    FutureUse4: string,
    FutureUse5: string,
    Designation: string,
    ErmId: string,
    BandCode: string,
    LeCode: string,
    PpcProcessName: string,
    Qualification: string,
    EmployeeStatus: string,
    SupervisorName: string,
    Pole: string,
    Sdo: string,
    LocationCode: string,
    DateExit: string,
    SupervisorEmail: string,
    EmpCode: string,
    EfmfmProcessId: string,
    Loc: string,
    EmpEmployment: string,
    EfmfmCity: string,
    Department: string,
    UserPersonType: string,
    CountryName: string,
    DataPort: string,
    VoicePort: string,
    EmergencyContactName: string,
    EmergencyContactNumber: string,
    PfAccountNumber: string,
    UniversalAccountNumber: string,
    AddressWheelsSite: string,
    SecondaryEmailId: string,
    Domain: string,
    WfhStatus: string,
    LastUpdateDate: string,
    DOB: string,
    HrManagerNumber: string,
    LastIncrementDate: string,
    NextIncrementDueDate: string,
    CurrDate: string,
    Skills: string,
    HRM_Name: string,
    ADHAAR: string,
    MARITAL_STATUS: string,
    LE_NAME: string
  }
}