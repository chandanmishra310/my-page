import { Component,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { IonicPage, NavController, NavParams ,App,AlertController,ViewController} from 'ionic-angular';
import {DashboardPage} from '../dashboard/dashboard';
import {LoginPage} from '../login/login';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
 import { InAppBrowser } from '@ionic-native/in-app-browser';
 
/**
 * Generated class for the AppDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-dashboard',
  templateUrl: 'app-dashboard.html',
})
export class AppDashboardPage {
   showBtn: boolean = false;
   deferredPrompt;
  //preferred_username:string;
  OHRID:string = this.name.substring(0,9);

  constructor(private  iab:InAppBrowser,private oauthService: OAuthService,public viewCtrl:ViewController,public navCtrl: NavController, public navParams: NavParams,public alertCtrl:AlertController,public app: App) {
  localStorage.setItem("emailname",this.OHRID);
  console.log("emailname>>>>>>>>>>>>>",this.name);
  console.log("OHRID>>>>>>>>>>>>>",this.OHRID);
  console.log("claims>>>>>>>>>>>>>",this.claims);
  localStorage.setItem("2","timeout");
  }
  openChatbot()
{
const browser = this.iab.create('https://hr.genpact.com/hr/peopleFirst/?env=cpub');
browser.show()
}
  // ionViewWillEnter(){
  //   window.addEventListener('beforeinstallprompt', (e) => {
  //     // Prevent Chrome 67 and earlier from automatically showing the prompt
  //     e.preventDefault();
  //     // Stash the event so it can be triggered later on the button event.
  //     this.deferredPrompt = e;
  //   // Update UI by showing a button to notify the user they can add to home screen
  //     this.showBtn = true;
  //   });
     
  //   //button click event to show the promt
             
  //   window.addEventListener('appinstalled', (event) => {
  //    alert('installed');
  //   });
     
     
  //   if (window.matchMedia('(display-mode: standalone)').matches) {
  //     alert('display-mode is standalone');
  //   }
  // }
  // add_to_home(e){
  //   debugger
  //   // hide our user interface that shows our button
  //   // Show the prompt
  //   this.deferredPrompt.prompt();
  //   // Wait for the user to respond to the prompt
  //   this.deferredPrompt.userChoice
  //     .then((choiceResult) => {
  //       if (choiceResult.outcome === 'accepted') {
  //         alert('User accepted the prompt');
  //       } else {
  //         alert('User dismissed the prompt');
  //       }
  //       this.deferredPrompt = null;
  //     });
  // };
  get name() {
    const claims: any = this.oauthService.getIdentityClaims();
    if (!claims) {
      return null;
    }
    return claims.preferred_username;
  }
  get claims() {
    return this.oauthService.getIdentityClaims();
  }
goNext(){
 this.navCtrl.push(DashboardPage);
}

logout() {
  const confirm = this.alertCtrl.create({
    title: 'MYPAGE',
    message: 'Do you wish to Sign Out?',
    buttons: [
      {
        text: 'NO',
        handler: () => {
          console.log('Disagree clicked');
        }
      },
      {
        text: 'YES',
        handler: () => {
          this.oauthService.logOut();
          // const root = this.app.getRootNav();
          // root.popToRoot();
          //   this.navCtrl.setRoot(LoginPage);

          //  this.navCtrl.popToRoot();
          // console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');

        }
      }
    ]
  });

  confirm.present();
}
  ionViewDidLoad() {
    if (!this.oauthService.hasValidIdToken()) {
      this.navCtrl.push('LoginPage');
    }
    console.log('ionViewDidLoad AppDashboardPage');
  }

}
