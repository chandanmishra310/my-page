import { NgModule,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppDashboardPage } from './app-dashboard';

@NgModule({
  declarations: [
    AppDashboardPage,
  ],
  imports: [
    IonicPageModule.forChild(AppDashboardPage),
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppDashboardPageModule {}
