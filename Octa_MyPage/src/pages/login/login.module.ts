import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';
import { Content } from 'ionic-angular';

@NgModule({
  declarations: [
    LoginPage,
  ],

  imports: [
    IonicPageModule.forChild(LoginPage),
  ],
})

export class LoginPageModule {
  // public showNavbar: boolean;
  // // ...
  // public hideNavbar(): void {
  //   this.showNavbar = false;
    // You should resize the content to use the space left by the navbar
    // this.content.resize();
  // }
}