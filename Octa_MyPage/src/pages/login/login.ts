import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, App, NavParams, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
// import { DashboardPage } from '../dashboard/dashboard';
import { AppDashboardPage } from '../app-dashboard/app-dashboard';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

  public user_email: any;
  credentialsForm: FormGroup;
  showResultMessage = false;
  message: string;
  userIsLoggedIn: any = true;

  constructor(private oauthService: OAuthService, private app: App, public navCtrl: NavController, private formBuilder: FormBuilder, public navParams: NavParams, public alertCtrl: AlertController, public modalCtrl: ModalController) {
    // this.credentialsForm = this.formBuilder.group({
    //   user_email: ["", Validators.required],
    // });
    if (this.oauthService.hasValidIdToken()) {
      this.app.getRootNavs()[0].setRoot(AppDashboardPage);
    }
    oauthService.redirectUri = window.location.origin;
     //oauthService.clientId = '0oag8thlrGxEPCyJp0x5';
     //oauthService.issuer = 'https://genpact.oktapreview.com';
    
   oauthService.clientId = '0oafnf34dwtKkQu9H0h7';
    oauthService.scope = 'openid profile email';
    oauthService.issuer = 'https://dev-342104.oktapreview.com/oauth2/default';
    oauthService.tokenValidationHandler = new JwksValidationHandler();
    oauthService.loadDiscoveryDocumentAndTryLogin();
  }
  // ionViewCanEnter(): boolean {
  //   if(this.userIsLoggedIn){
  //     alert("Alert1");
  //     this.oauthService.initImplicitFlow();
  //     alert("Alert2");
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad LoginPage');
  //   this.user_email = "";
  //   // if(this.userIsLoggedIn){
  //   //   alert("Alert1");
  //   //   this.oauthService.initImplicitFlow();
  //   //   alert("Alert2");
  //   //  this.userIsLoggedIn=false;
  //   // }
  // }

  // ionViewWillEnter() {
  //   this.credentialsForm.reset();
  // }
  // presentAlert() {
  //   let alert = this.alertCtrl.create({
  //     title: 'Information',
  //     subTitle: 'Please enter all information',
  //     buttons: ['Dismiss']
  //   });
  //   alert.present();
  // }

  logForm() {
    this.oauthService.initImplicitFlow();
  }

  // verify() {
  //   this.showResultMessage = false;
  //   if (this.credentialsForm.valid) {
  //     this.user_email = this.credentialsForm.controls.user_email.value;

  //     // alert(this.user_email);
  //     if ((this.user_email == "703181797") || (this.user_email == "303007750") || (this.user_email == "703011504") || (this.user_email == "303027869") || (this.user_email == "302011033") || (this.user_email == "303003914") || (this.user_email == "703181403") || (this.user_email == "703007911") || (this.user_email == "303008989") || (this.user_email == "703010818")) {

  //       // 303027869
  //       //303007750
  //       //alert("Successfully Login.")

  //       // if (this.user_password != "welcome1") {
  //       //   this.showResultMessage = true;
  //       //   this.message = "Please enter valid password.";
  //       // }

  //       // else {
  //       let data = {
  //         loginInfo: this.user_email
  //       };

  //       //   localStorage.setItem("OHRID", this.user_email);
  //       //   /// this.navCtrl.setRoot(LoginPage);
  //       //   this.user_email = "";
  //       //   this.user_password = "";
  //       //   //this.navCtrl.push(AppDashboardPage, data);
  //       //   this.navCtrl.setRoot(AppDashboardPage, data);

  //       // }

  //       localStorage.setItem("OHRID", this.user_email);
  //       // this.navCtrl.push(AppDashboardPage, data);
  //       // this.navCtrl.setRoot(AppDashboardPage, data);

  //       this.logForm();
  //     } else {
  //       this.showResultMessage = true;
  //       this.message = "Please enter valid OHRID";
  //     }

  //   } else {
  //     Object.keys(this.credentialsForm.controls).forEach(field => { // {1}
  //       const control = this.credentialsForm.get(field);            // {2}
  //       control.markAsTouched({ onlySelf: true });       // {3}
  //     });
  //   }

  // }

}