import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,App} from 'ionic-angular';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { LoadingController } from 'ionic-angular';
import { RequestOptions } from '@angular/http';
import {TabsPage} from '../tabs/tabs';
import {LoginPage} from '../login/login';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { DashboardPage } from '../dashboard/dashboard';




/**
 * Generated class for the ElBalancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-el-balance',
  templateUrl: 'el-balance.html',
})
export class ElBalancePage {

  //OHRID = '302011033';
  maxtime = 20;
  timer: number;
  timeout = 0;
  fName:string;
  lName:string;
  fullName:string;
  //OHRID: string = localStorage.getItem("OHRID");
  OHRID:string = localStorage.getItem("emailname");
  static baseUrl: string = 'https://genpacticsinstancedev01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/';
  static getEmployeeUrl: string = ElBalancePage.baseUrl + 'REQUEST_LEAVE_GET_API/v01/EarnedLeaves?OHRID=';
  static userName = 'rgupta';
  static password = 'O@acle@123';
  name: string;
  leaveType: string;
  hrmName: string;
  hrmOHR: string;
  supervisorName: string;
  earnedLeaveBalance:string;
  encodedString: string;
  employeeDetailsResponse: EarnedLeavesDetail;
  constructor(private oauthService: OAuthService,public alertCtrl:AlertController,public app: App,public navCtrl: NavController, public loadingCtrl: LoadingController, public navParams: NavParams, private http: HttpClient)
   {
    console.log('ohrid =====>');
    console.log(this.OHRID);
    console.log('navCtrl ==>');
    console.log(navCtrl);
    this.getAuthorizationKey();
    this.getEmployeeUrl();
  }
  ionViewDidLoad(){
    
    if(localStorage.getItem("fName") == null)
{
  this.fullName = "";
}else{
    this.fName = localStorage.getItem("fName");
    this.lName = localStorage.getItem("lName");
    this.fullName = this.fName + "," + this.lName;
  }
}

  goProfile()
{
  this.navCtrl.push(TabsPage);
}
  logout() {
    const confirm = this.alertCtrl.create({
      title: 'MYPAGE',
      message: 'Do you wish to Sign Out?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YES',
          handler: () => {
            this.oauthService.logOut();
            // const root = this.app.getRootNav();
            // root.popToRoot();
            //   this.navCtrl.setRoot(LoginPage);
  
            // this.navCtrl.popToRoot();
            // console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');
  
          }
        }
      ]
    });
    confirm.present();
  }
  getAuthorizationKey() {
    const encodedString = btoa(ElBalancePage.userName + ":" + ElBalancePage.password);
    console.log('encodedString ==>');
    console.log(encodedString);
    this.encodedString = encodedString;
  }
  getEmployeeUrl() {
    let loader = this.loadingCtrl.create({
     // content: 'Getting latest entries...',
     spinner: 'hide',
     content: '<img src="assets/imgs/logogif.gif"/>',
    });

    // Show the popup
    this.StartTimer(loader);
    loader.present();

    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '850027873')
      .set('SecretKey', '29fb9858c6eed8ef');

    this.http.get(ElBalancePage.getEmployeeUrl + this.OHRID
      , { headers }).subscribe(
        res => {
          this.timeout = 1;
          console.log(res);
          // this.firstName = res.EmployeeDetail.FirstName;
          this.employeeDetailsResponse = <EarnedLeavesDetail>res;
          console.log('this.employeeDetailsResponse.EmployeeDetail.FirstName ==>');

          //console.log(this.employeeDetailsResponse.EarnedLeavesDetail.FirstName);

          //this.name = this.employeeDetailsResponse.EarnedLeavesDetail.EmployeeId;
          //this.leaveType = this.employeeDetailsResponse.EarnedLeavesDetail.FirstName;
          this.hrmName = this.employeeDetailsResponse.EarnedLeavesDetail.HRMName;
          this.hrmOHR = this.employeeDetailsResponse.EarnedLeavesDetail.HRMOHR;
          this.supervisorName = this.employeeDetailsResponse.EarnedLeavesDetail.SupervisorName;
          this.earnedLeaveBalance = this.employeeDetailsResponse.EarnedLeavesDetail.EarnedLeaveBalance;
          console.log('this.employeeDetailsResponse.EmployeeDetail.EarnedLeaveBalance ==>');
          localStorage.setItem("supervisor",this.supervisorName);
        },
        err => {
          console.error(err);
        },

        () => {
          console.log('Request complete');
          loader.present().then(() => {

            loader.dismiss();
          });
        }
      );
  }
  // ionViewDidLoad() {

  //   console.log('ionViewDidLoad ElBalancePage');
  // }
  StartTimer(loader) {
    this.timer = setTimeout(x => {
      if (this.maxtime <= 0) {
      }
      this.maxtime -= 1;
      if (this.maxtime > 0) {
        this.StartTimer(loader);
        console.log(this.maxtime);
      }
      else {
        if (this.timeout == 0) {
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
          // localStorage.setItem("1","timeout")
        }
      }
    }, 1000);
  }
}
export interface EarnedLeavesDetail {
  EarnedLeavesDetail: {
    HRMName: string,
    HRMOHR: string,
    SupervisorName: string,
    EarnedLeaveBalance:string;
     }
}
