import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElBalancePage } from './el-balance';

@NgModule({
  declarations: [
    ElBalancePage,
  ],
  imports: [
    IonicPageModule.forChild(ElBalancePage),
  ],
})
export class ElBalancePageModule {}
