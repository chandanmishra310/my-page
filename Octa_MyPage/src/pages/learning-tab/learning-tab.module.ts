import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LearningTabPage } from './learning-tab';
import { MandatoryTrainigsPage } from '../mandatory-trainigs/mandatory-trainigs';
import { PastDueTrainingsPage } from '../past-due-trainings/past-due-trainings';
import { ActiveTrainigsPage }  from '../active-trainigs/active-trainigs';

@NgModule({
  declarations: [
    LearningTabPage,
    MandatoryTrainigsPage,
    PastDueTrainingsPage,
    ActiveTrainigsPage
  ],
  imports: [
    IonicPageModule.forChild(LearningTabPage),
  ]
})
export class LearningTabPageModule {}
