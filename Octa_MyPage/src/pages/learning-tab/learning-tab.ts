import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';
import { NgModule } from '@angular/core';
import { MandatoryTrainigsPage } from '../mandatory-trainigs/mandatory-trainigs';
import { PastDueTrainingsPage } from '../past-due-trainings/past-due-trainings';
import { ActiveTrainigsPage } from '../active-trainigs/active-trainigs';
import { ChartsModule } from 'ng2-charts';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { MyApp } from '../../app/app.component';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';
import 'rxjs/add/observable/of';
import 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { LoadingController } from 'ionic-angular';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { TabsPage } from '../tabs/tabs';
import {LoginPage} from '../login/login';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';



@IonicPage()
@Component({
  selector: 'page-learning-tab',
  templateUrl: 'learning-tab.html'
})
export class LearningTabPage {

  public pieChartLabels: string[] = ['COMPLETED', 'IN PROGRESS', 'DUE'];

  public myPage: MandatoryTrainigsPage;

  public pieChartType: string = 'pie';


  progress: any = localStorage.getItem('progress');
  completed: any = localStorage.getItem('completed');
  registered: any = localStorage.getItem('registered');

  public pieChartData: number[] = [this.progress, this.completed, this.registered];


  myTrainingsCollection: MyTrainingsCollection[] = [];
  myTrainings: MyTrainings;
  pieChartOptions: any;
  //OHRID: string = '302011033';
  OHRID:string = localStorage.getItem("emailname");
  encodedString: string;

  static baseUrl: string = 'https://genpacticsinstancedev01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/';
  static myTrainingUrl: string = LearningTabPage.baseUrl + 'ILEARN_MYTRAINING_API/v01/myTrainings?OHRID=';
  static userName = 'rgupta';
  static password = 'O@acle@123';

  mandatoryTrainigsRoot = MandatoryTrainigsPage
  activeTrainigsRoot = ActiveTrainigsPage
  pastDueTrainingsRoot = PastDueTrainingsPage

  tab2Params: any; //{ id: this.myTrainingsCollection };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private oauthService: OAuthService,public navCtrl: NavController, private alertCtrl: AlertController, public app: App, private http: HttpClient, public loadingCtrl: LoadingController)
   {
    // this.getAuthorizationKey();
    //this.getEmployeeUrl();
    this.pieChartOptions = {
      // segmentShowStroke: '#000', 
      segmentStrokeColor: '#000',
      pieceLabel: {
        render: 'label',
        fontColor: ['green', 'white', 'red'],
      },
      legend: {
        display: false,
        elements: {
          arc: {
            borderWidth: 0
          }
        }
      },
    }
  }

  goProfile()
{
  this.navCtrl.push(TabsPage);
}
  
  
  logout() {
    const confirm = this.alertCtrl.create({
      title: 'MYPAGE',
      message: 'Do you wish to Sign Out?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YES',
          handler: () => {
            this.oauthService.logOut();
            // const root = this.app.getRootNav();
            // root.popToRoot();
            //   this.navCtrl.setRoot(LoginPage);
  
            //  this.navCtrl.popToRoot();
            // console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');
  
          }
        }
      ]
    });
  
    confirm.present();
  }

  getAuthorizationKey() {
    const encodedString = btoa(LearningTabPage.userName + ":" + LearningTabPage.password);
    console.log('encodedString ==>');
    console.log(encodedString);
    this.encodedString = encodedString;
  }

  getEmployeeUrl() {
    let loader = this.loadingCtrl.create({
      //content: 'Getting latest entries...',
      spinner: 'hide',
      content: '<img src="assets/imgs/logogif.gif"/>',
    });

    //Show the popup
    loader.present();

    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '850027873')
      .set('SecretKey', '29fb9858c6eed8ef');
    let result: string[];
    let hh: string;
    let mm: string;
    let Url: string;
    let newUrl: string;

    this.http.get(LearningTabPage.myTrainingUrl + this.OHRID
      , { headers }).subscribe(
        res => {
          console.log(res);
          // this.firstName = res.EmployeeDetail.FirstName;
          this.myTrainings = <MyTrainings>res;
          console.log('this.myTrainings.MyTrainingsCollection ==>');

          console.log(this.myTrainings);
          this.myTrainingsCollection = this.myTrainings.MyTrainings.MyTrainingsCollection.map(function (value, index, array) {
            let genp: string = "https://genpact.csod.com/lms/scorm/clientLMS/ScormFrames.aspx?";
            result = value.TotalTrainingHours.split(":");
            Url = value.TrainingURL;
            newUrl = Url + genp;
                        hh = result[0].slice(2, result[0].length);
                        mm = result[1];
                        value.TotalTrainingHours = hh + ':' + mm;
                        result = value.TotalTime.split(":");
                        hh = result[0].slice(2, result[0].length);
                        mm = result[1];
                        value.TotalTime = hh + ':' + mm;
            return value;
          }, this.myTrainingsCollection);

          this.tab2Params = { id: this.myTrainingsCollection };
          console.log('My console data' + this.tab2Params);

        },
        err => {
          console.error(err);
        },

        () => {
          console.log('Request complete');
          loader.present().then(() => {

            loader.dismiss();
          });
        }
      );
  }
}



export interface MyTrainingsCollection {
  OHRID: string,
  TrainingID: string,
  TrainingTitle: string,
  TrainingCompletionDate: string,
  Status: string,
  DueDate: string,
  TotalTime: string,
  TotalTrainingHours: string,
  TrainingScore: string,
  Provider: string,
  TrainingURL: string
}

export interface MyTrainings {
  MyTrainings: {
    MyTrainingsCollection: MyTrainingsCollection[]
  }
}