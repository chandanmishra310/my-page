import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ViewController } from 'ionic-angular';

/**
 * Generated class for the HelpmetModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-helpmet-modal',
  templateUrl: 'helpmet-modal.html',
})
export class HelpmetModalPage {

  reqest_num:string = localStorage.getItem("reqest_num");
  reqest_name:string = localStorage.getItem("reqest_name");
  reqest_ohr:string = localStorage.getItem("reqest_ohr");
  base_location:string = localStorage.getItem("base_location");
  supervisor:string = localStorage.getItem("supervisor");
  details:string = localStorage.getItem("details");
  bandCode:string = localStorage.getItem("bandCode");
  assAttribute21:string = localStorage.getItem("assAttribute21");
  assAttribute22:string = localStorage.getItem("assAttribute22");
  leCode:string = localStorage.getItem("leCode");
  supervisorName:string = localStorage.getItem("supervisorName");


  constructor(public viewCtrl: ViewController,public navCtrl: NavController, public navParams: NavParams) {
    
    //console.log(this.details+'details HelpmetModalPage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpmetModalPage');
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
}
