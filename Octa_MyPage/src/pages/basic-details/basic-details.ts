import { Component, ViewChild } from '@angular/core';
import { ToastController, NavController, AlertController, NavPop, App, NavParams } from 'ionic-angular';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { RequestOptions } from '@angular/http';
import { MyApp } from '../../app/app.component';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';
import 'rxjs/add/observable/of';
import 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { LoadingController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { OktaLoginPage } from '../okta-login/okta-login';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { TabsPage } from '../tabs/tabs';
import { AppDashboardPage } from '../app-dashboard/app-dashboard';
import { DashboardPage } from '../dashboard/dashboard';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Time } from '@angular/common';

/**
 * Generated class for the BasicDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-basic-details',
  templateUrl: 'basic-details.html',
})
export class BasicDetailsPage {
  @ViewChild('myNav') nav: NavController
  // myTrainingsCollection: PFNumbers[] = [];
  // myTrainingsUAN: UANNumbers[] = [];
  // myTrainings: PFNumbersCollection;
  // myUAN: UANNumbersCollection;

  //OHRID: string = localStorage.getItem("OHRID");
  OHRID:string = localStorage.getItem("emailname");
 // OHRID: string = '302011033';

  static baseUrl_PF: string = 'https://genpacticsinstancetest01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/';
  static getEmployeePF: string = BasicDetailsPage.baseUrl_PF + 'PF_UAN_NUMBER_API/v01/getPFUANNumber?OHRID=';

  static baseUrl: string = 'https://genpacticsinstancedev01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/';
  static getEmployeeUrl: string = BasicDetailsPage.baseUrl + 'EMPLOYEE_DETAILS_INTEGRATION/v01/getEmployeeData?OHRID=';
  static myTrainingUrl: string = BasicDetailsPage.baseUrl + 'ILEARN_MYTRAINING_API/v01/myTrainings?OHRID=';
  static userName = 'rgupta';
  static password = 'O@acle@123';
  maxtime = 20;
  timer: number;
  timeout = 0;
  employeeDetailsResponse: EmployeeDetails;
  employeeId: string;
  firstName: string = "";
  lastName: string = "";
  originalDateOfHire: string;
  bandCode: string;
  designation: string;
  department: string;
  supervisorName: string;
  locationCode: string;
  locationId: string;
  gender: string;
  dob: string;
  email: string;
  address1: string;
  address2: string;
  postalCode: string;
  region: string;
  country: string;
  encodedString: string;
  pfNumberFirst: string;
  pfNumberSecond: string;
  uanNumber: string;
  pfNumberFs: string;
  pfNumberSc: string;
  show: boolean = true ;

  constructor(private toastCtrl: ToastController, public navCtrl: NavController, private http: HttpClient, public app: App, public loadingCtrl: LoadingController, public navParams: NavParams) {
    console.log('ohrid =====>');
    console.log(this.OHRID);
    console.log('navCtrl ==>');
    console.log(navCtrl);
    let PFNumber: string;
    this.getAuthorizationKey();
    this.getEmployeeUrl();
    this.getEmployeePF();
   // localStorage.setItem("2", "timeout")

  }
  getAuthorizationKey() {
    const encodedString = btoa(BasicDetailsPage.userName + ":" + BasicDetailsPage.password);
    console.log('encodedString ==>');
    console.log(encodedString);
    this.encodedString = encodedString;
  }
  
  getEmployeePF() {
    let loader = this.loadingCtrl.create({
      spinner: 'hide',
      content: '<img src="assets/imgs/logogif.gif"/>',
    });

    //Show the popup
    this.StartTimer(loader);
    loader.present();

    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '850027873')
      .set('SecretKey', '29fb9858c6eed8ef');
    let result: string[];
    let hh: string;
    let mm: string;
    let Url: string;
    let newUrl: string;
    let PFNumber: string;

    this.http.get(BasicDetailsPage.getEmployeePF + this.OHRID
      , { headers }).subscribe(
        res => {
          this.timeout = 1;
          loader.dismiss();
          let pfNumberCollections = res["PFNumbersCollection"]
          let pfNmberArrray = pfNumberCollections.PFNumbers;

          let pfNmberDict = pfNmberArrray[0];
          this.pfNumberFirst = pfNmberDict["PFNumber"];
          this.pfNumberFs = this.pfNumberFirst.substring(this.pfNumberFirst.length - 5, this.pfNumberFirst.length - 0);

          let pfNmberDictSecond = pfNmberArrray[1];
          this.pfNumberSecond = pfNmberDictSecond["PFNumber"];
          this.pfNumberSc = this.pfNumberSecond.substring(this.pfNumberSecond.length - 5, this.pfNumberSecond.length - 0);

          let uanNumberCollections = res["UANNumbersCollection"];
          let uanNmberArrray = uanNumberCollections.UANNumbers;
          let uanNmberDict = uanNmberArrray[0];
          this.uanNumber = uanNmberDict["UANNumber"];

          if (this.pfNumberFs === this.pfNumberSc) {
            
            this.show = false;
        
          }
        },
        err => {
          console.error(err);
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
        },

        // () => {
        //   console.log('Request complete');
        //   loader.present().then(() => {
        //     loader.dismiss();
        //   });
        // }
      );
  }
  getEmployeeUrl() {

    //localStorage.setItem("2", "timeout")
    let loader = this.loadingCtrl.create({
      cssClass: 'myClass',
      spinner: 'hide',
      content: '<img src="assets/imgs/logogif.gif"/>',
    });

    this.StartTimer(loader);
    // Show the popup
    loader.present();
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '850027873')
      .set('SecretKey', '29fb9858c6eed8ef');

    this.http.get(BasicDetailsPage.getEmployeeUrl + this.OHRID
      , { headers }).subscribe(
        res => {
          console.log(res);
          this.timeout = 1;
          loader.dismiss();
          this.employeeDetailsResponse = <EmployeeDetails>res;
          this.employeeId = this.employeeDetailsResponse.EmployeeDetail.EmployeeId;
          this.firstName = this.employeeDetailsResponse.EmployeeDetail.FirstName;
          this.lastName = this.employeeDetailsResponse.EmployeeDetail.LastName;
          this.originalDateOfHire = this.employeeDetailsResponse.EmployeeDetail.OriginalDateOfHire;
          this.bandCode = this.employeeDetailsResponse.EmployeeDetail.BandCode;
          this.designation = this.employeeDetailsResponse.EmployeeDetail.Designation;
          this.supervisorName = this.employeeDetailsResponse.EmployeeDetail.SupervisorName;
          this.locationCode = this.employeeDetailsResponse.EmployeeDetail.LocationCode;
          this.locationId = this.employeeDetailsResponse.EmployeeDetail.LocationId;
          this.gender = this.employeeDetailsResponse.EmployeeDetail.Sex;
          this.dob = this.employeeDetailsResponse.EmployeeDetail.DOB;
          this.email = this.employeeDetailsResponse.EmployeeDetail.EmailAdd;
          this.address1 = this.employeeDetailsResponse.EmployeeDetail.Line1;
          this.address2 = this.employeeDetailsResponse.EmployeeDetail.Line2;
          this.postalCode = this.employeeDetailsResponse.EmployeeDetail.PostalCode;
          this.department = this.employeeDetailsResponse.EmployeeDetail.Department;
          this.region = this.employeeDetailsResponse.EmployeeDetail.Region2;
          this.country = this.employeeDetailsResponse.EmployeeDetail.Country;
          localStorage.setItem("fName", this.firstName);
          localStorage.setItem("lName", this.lastName);
        },
        err => {
          console.error(err);
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
        },
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BasicDetailsPage');
  }

  StartTimer(loader) {
    this.timer = setTimeout(x => {
      if (this.maxtime <= 0) {
      }
      this.maxtime -= 1;
      if (this.maxtime > 0) {
        this.StartTimer(loader);
        console.log(this.maxtime);
      }
      else {
        if (this.timeout == 0) {
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
          this.presentToast();
        }
      }
    }, 1000);
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Internet Conection is lost ',
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
export interface EmployeeDetails {
  EmployeeDetail: {
    EmployeeId: string,
    IlearnActive: string,
    Absent: string,
    AllowReconcile: string,
    Title: string,
    FirstName: string,
    MiddleName: string,
    LastName: string,
    Suffix: string,
    EmailAdd: string,
    PhoneWork: string,
    PhoneHome: string,
    PhoneNumber: string,
    PhoneFax: string,
    Country: string,
    Line1: string,
    Line2: string,
    Region2: string,
    PostalCode: string,
    PoleCode: string,
    LocationId: string,
    JobId: string,
    AssAttribute25: string,
    GradeId: string,
    DateStart: string,
    OriginalDateOfHire: string,
    OrganizationApprovals: string,
    IlearnApprover: string,
    ManagerEmployeeNum: string,
    Sex: string,
    GetLookupMeaning: string,
    PersonTypeId: string,
    BusinessGroupId: string,
    AssAttribute21: string,
    AssAttribute22: string,
    AssAttribute24: string,
    OrganizationId: string,
    ServiceLine: string,
    AccountCode: string,
    ProductId: string,
    ProductFamilyId: string,
    InternalIndustryViewL1: string,
    FpnaOhr: string,
    AvpOhr: string,
    UnigenUser: string,
    HireType: string,
    ServiceAgreementType: string,
    GdcFlag: string,
    BizSeg: string,
    BusinessSegments: string,
    HrManager: string,
    MatrixManager: string,
    TrainingManager: string,
    TrainingLeader: string,
    ProgramInitiativeManager1: string,
    Contractor: string,
    LoginId: string,
    LocaleCode: string,
    CountryCode: string,
    LedgerCode: string,
    ReimbCurrCode: string,
    CashAdvAcc: string,
    OrgUnit1: string,
    OrgUnit2: string,
    OrgUnit3: string,
    Custom1: string,
    Custom9: string,
    Custom10: string,
    Custom11: string,
    Custom12: string,
    Custom13: string,
    Custom14: string,
    Custom15: string,
    Custom16: string,
    Custom21: string,
    Traveler: string,
    BiManager: string,
    TravelReqUser: string,
    FutureUse2: string,
    CliqbookUser: string,
    FutureUse3: string,
    FutureUse4: string,
    FutureUse5: string,
    Designation: string,
    ErmId: string,
    BandCode: string,
    LeCode: string,
    PpcProcessName: string,
    Qualification: string,
    EmployeeStatus: string,
    SupervisorName: string,
    Pole: string,
    Sdo: string,
    LocationCode: string,
    DateExit: string,
    SupervisorEmail: string,
    EmpCode: string,
    EfmfmProcessId: string,
    Loc: string,
    EmpEmployment: string,
    EfmfmCity: string,
    Department: string,
    UserPersonType: string,
    CountryName: string,
    DataPort: string,
    VoicePort: string,
    EmergencyContactName: string,
    EmergencyContactNumber: string,
    PfAccountNumber: string,
    UniversalAccountNumber: string,
    AddressWheelsSite: string,
    SecondaryEmailId: string,
    Domain: string,
    WfhStatus: string,
    LastUpdateDate: string,
    DOB: string,
    HrManagerNumber: string,
    LastIncrementDate: string,
    NextIncrementDueDate: string,
    CurrDate: string,
    Skills: string,
    HRM_Name: string,
    ADHAAR: string,
    MARITAL_STATUS: string,
    LE_NAME: string
  }
}
