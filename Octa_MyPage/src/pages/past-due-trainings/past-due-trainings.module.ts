import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PastDueTrainingsPage } from './past-due-trainings';

@NgModule({
  declarations: [
    PastDueTrainingsPage,
  ],
  imports: [
    IonicPageModule.forChild(PastDueTrainingsPage),
  ],
})
export class PastDueTrainingsPageModule {}
