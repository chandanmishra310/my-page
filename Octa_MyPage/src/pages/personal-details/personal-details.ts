import { Component } from '@angular/core';
import { NavController, AlertController, NavPop,App,NavParams } from 'ionic-angular';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { RequestOptions } from '@angular/http';
import { MyApp } from '../../app/app.component';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';
import 'rxjs/add/observable/of';
import 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { LoadingController } from 'ionic-angular';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { LoginPage } from '../login/login';
import { DashboardPage } from '../dashboard/dashboard';

/**
 * Generated class for the PersonalDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-personal-details',
  templateUrl: 'personal-details.html',
})
export class PersonalDetailsPage {

  //OHRID: string = localStorage.getItem("OHRID");
  OHRID:string = localStorage.getItem("emailname");
  //OHRID: string ='302011033';
  maxtime = 20;
  timer: number;
  timeout = 0;
  static baseUrl: string = 'https://genpacticsinstancedev01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/';
  static getEmployeeUrl: string = PersonalDetailsPage.baseUrl + 'EMPLOYEE_DETAILS_INTEGRATION/v01/getEmployeeData?OHRID=';
  static myTrainingUrl: string = PersonalDetailsPage.baseUrl + 'ILEARN_MYTRAINING_API/v01/myTrainings?OHRID=';
  static userName = 'rgupta';
  static password = 'O@acle@123';

  employeeDetailsResponse: EmployeeDetails;
  employeeId: string;
  firstName: string;
  lastName: string;
  originalDateOfHire:string;
  bandCode:string;
  designation:string;
  department: string;
  emergencyContactNumber:string;
  addressWheelsSite:string;
  supervisorName:string;
  locationCode:string;
  locationId:string;
  gender: string;
  dob: string;
  email: string;
  address1: string;
  address2: string;
  postalCode: string;
  secondaryEmailId:string;
  voicePort:string;
  dataPort:string;
  region: string;
  country: string;
  domain:string;
  lE_NAME:string;
  encodedString: string;
  constructor(public navCtrl: NavController, private http: HttpClient,public app:App,public loadingCtrl: LoadingController,public navParams: NavParams) {
    console.log('ohrid =====>');
    console.log(this.OHRID);
    console.log('navCtrl ==>');
    console.log(navCtrl);
    
  }
 


  getAuthorizationKey() {
    const encodedString = btoa(PersonalDetailsPage.userName + ":" + PersonalDetailsPage.password);
    console.log('encodedString ==>');
    console.log(encodedString);
    this.encodedString = encodedString;
  }
  getEmployeeUrl() {
    let loader = this.loadingCtrl.create({
      //content: 'Getting latest entries...',
      spinner: 'hide',
      content: '<img src="assets/imgs/logogif.gif"/>',
      //duration: 10000,
    });

    // Show the popup
    this.StartTimer(loader);
    loader.present();
   
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '850027873')
      .set('SecretKey', '29fb9858c6eed8ef');
    this.http.get(PersonalDetailsPage.getEmployeeUrl+this.OHRID
      , { headers }).subscribe(
        res => {
          //console.log(res);
         // this.firstName = res.EmployeeDetail.FirstName;
          this.employeeDetailsResponse = <EmployeeDetails>res;
         // console.log('this.employeeDetailsResponse.EmployeeDetail.FirstName ==>');
          //console.log(this.employeeDetailsResponse.EmployeeDetail.FirstName);
          this.timeout = 1;
          this.lE_NAME = this.employeeDetailsResponse.EmployeeDetail.LE_NAME;
          if(this.lE_NAME == "")
          {
            this.lE_NAME = 'NA';
          }
          this.domain = this.employeeDetailsResponse.EmployeeDetail.Domain;
          if(this.domain == "")
          {
            this.domain = 'NA';
          }
          this.dataPort = this.employeeDetailsResponse.EmployeeDetail.DataPort; 
          if(this.dataPort == "")
          {
            this.dataPort = 'NA';
          }
          this.voicePort = this.employeeDetailsResponse.EmployeeDetail.VoicePort;
          if(this.voicePort == "")
          {
            this.voicePort = 'NA';
          }
          this.secondaryEmailId = this.employeeDetailsResponse.EmployeeDetail.SecondaryEmailId;
          if(this.secondaryEmailId == "")
          {
            this.secondaryEmailId = 'NA';
          }
          this.addressWheelsSite = this.employeeDetailsResponse.EmployeeDetail.AddressWheelsSite;
          if(this.addressWheelsSite == "")
          {
            this.addressWheelsSite = 'NA';
          }
          this.emergencyContactNumber = this.employeeDetailsResponse.EmployeeDetail.EmergencyContactNumber;
          
          if(this.emergencyContactNumber == "")
          {
            this.emergencyContactNumber = 'NA';
          }
          this.employeeId = this.employeeDetailsResponse.EmployeeDetail.EmployeeId;
          this.firstName = this.employeeDetailsResponse.EmployeeDetail.FirstName;
          this.lastName = this.employeeDetailsResponse.EmployeeDetail.LastName;
          this.originalDateOfHire = this.employeeDetailsResponse.EmployeeDetail.OriginalDateOfHire;
          if(this.originalDateOfHire == "")
          {
            this.originalDateOfHire = 'NA';
          }
          this.bandCode = this.employeeDetailsResponse.EmployeeDetail.BandCode;
          this.designation = this.employeeDetailsResponse.EmployeeDetail.Designation;
          this.supervisorName = this.employeeDetailsResponse.EmployeeDetail.SupervisorName;
          this.locationCode = this.employeeDetailsResponse.EmployeeDetail.LocationCode;
          this.locationId = this.employeeDetailsResponse.EmployeeDetail.LocationId;
          this.gender = this.employeeDetailsResponse.EmployeeDetail.Sex;
          this.dob = this.employeeDetailsResponse.EmployeeDetail.DOB;
          this.email = this.employeeDetailsResponse.EmployeeDetail.EmailAdd;
          this.address1 = this.employeeDetailsResponse.EmployeeDetail.Line1;
          this.address2 = this.employeeDetailsResponse.EmployeeDetail.Line2;
          this.postalCode = this.employeeDetailsResponse.EmployeeDetail.PostalCode;
          this.department = this.employeeDetailsResponse.EmployeeDetail.Department;
          if(this.department == "")
          {
            this.department = 'NA';
          }
          this.region = this.employeeDetailsResponse.EmployeeDetail.Region2;
          this.country = this.employeeDetailsResponse.EmployeeDetail.Country;
        },
        err => {
          console.error(err);
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
        },

        () => {
          console.log('Request complete');
          loader.present().then(() => {

            loader.dismiss();
          });
        }
      );
  }
  ionViewWillEnter() {
    console.log('ionViewDidLoad PersonalDetailsPage');
    this.getAuthorizationKey();
    this.getEmployeeUrl();

  }
  ionViewDidLoad() {
   
  }

  StartTimer(loader) {
    this.timer = setTimeout(x => {
      if (this.maxtime <= 0) {
      }
      this.maxtime -= 1;
      if (this.maxtime > 0) {
        this.StartTimer(loader);
        console.log(this.maxtime);
      }
      else {
        if (this.timeout == 0) {
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
          // localStorage.setItem("1","timeout")
        }
      }
    }, 1000);
  }

}
export interface EmployeeDetails {
  EmployeeDetail: {
    EmployeeId: string,
    IlearnActive: string,
    Absent: string,
    AllowReconcile: string,
    Title: string,
    FirstName: string,
    MiddleName: string,
    LastName: string,
    Suffix: string,
    EmailAdd: string,
    PhoneWork: string,
    PhoneHome: string,
    PhoneNumber: string,
    PhoneFax: string,
    Country: string,
    Line1: string,
    Line2: string,
    Region2: string,
    PostalCode: string,
    PoleCode: string,
    LocationId: string,
    JobId: string,
    AssAttribute25: string,
    GradeId: string,
    DateStart: string,
    OriginalDateOfHire: string,
    OrganizationApprovals: string,
    IlearnApprover: string,
    ManagerEmployeeNum: string,
    Sex: string,
    GetLookupMeaning: string,
    PersonTypeId: string,
    BusinessGroupId: string,
    AssAttribute21: string,
    AssAttribute22: string,
    AssAttribute24: string,
    OrganizationId: string,
    ServiceLine: string,
    AccountCode: string,
    ProductId: string,
    ProductFamilyId: string,
    InternalIndustryViewL1: string,
    FpnaOhr: string,
    AvpOhr: string,
    UnigenUser: string,
    HireType: string,
    ServiceAgreementType: string,
    GdcFlag: string,
    BizSeg: string,
    BusinessSegments: string,
    HrManager: string,
    MatrixManager: string,
    TrainingManager: string,
    TrainingLeader: string,
    ProgramInitiativeManager1: string,
    Contractor: string,
    LoginId: string,
    LocaleCode: string,
    CountryCode: string,
    LedgerCode: string,
    ReimbCurrCode: string,
    CashAdvAcc: string,
    OrgUnit1: string,
    OrgUnit2: string,
    OrgUnit3: string,
    Custom1: string,
    Custom9: string,
    Custom10: string,
    Custom11: string,
    Custom12: string,
    Custom13: string,
    Custom14: string,
    Custom15: string,
    Custom16: string,
    Custom21: string,
    Traveler: string,
    BiManager: string,
    TravelReqUser: string,
    FutureUse2: string,
    CliqbookUser: string,
    FutureUse3: string,
    FutureUse4: string,
    FutureUse5: string,
    Designation: string,
    ErmId: string,
    BandCode: string,
    LeCode: string,
    PpcProcessName: string,
    Qualification: string,
    EmployeeStatus: string,
    SupervisorName: string,
    Pole: string,
    Sdo: string,
    LocationCode: string,
    DateExit: string,
    SupervisorEmail: string,
    EmpCode: string,
    EfmfmProcessId: string,
    Loc: string,
    EmpEmployment: string,
    EfmfmCity: string,
    Department: string,
    UserPersonType: string,
    CountryName: string,
    DataPort: string,
    VoicePort: string,
    EmergencyContactName: string,
    EmergencyContactNumber: string,
    PfAccountNumber: string,
    UniversalAccountNumber: string,
    AddressWheelsSite: string,
    SecondaryEmailId: string,
    Domain: string,
    WfhStatus: string,
    LastUpdateDate: string,
    DOB: string,
    HrManagerNumber: string,
    LastIncrementDate: string,
    NextIncrementDueDate: string,
    CurrDate: string,
    Skills: string,
    HRM_Name: string,
    ADHAAR: string,
    MARITAL_STATUS: string,
    LE_NAME: string
  }
}