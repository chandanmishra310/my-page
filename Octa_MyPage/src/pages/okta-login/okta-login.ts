import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App } from 'ionic-angular';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';

/**
 * Generated class for the OktaLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-okta-login',
  templateUrl: 'okta-login.html',
})
export class OktaLoginPage {

  constructor(private oauthService: OAuthService, private app: App,public navCtrl: NavController, public navParams: NavParams) {
    oauthService.redirectUri = window.location.origin;
    //oauthService.clientId = '0oag8thlrGxEPCyJp0x5';
    //oauthService.issuer = 'https://genpact.oktapreview.com';
    oauthService.clientId = '0oafnf34dwtKkQu9H0h7';
    oauthService.scope = 'openid profile email';
    oauthService.issuer = 'https://dev-342104.oktapreview.com/oauth2/default';
    oauthService.tokenValidationHandler = new JwksValidationHandler();
    oauthService.loadDiscoveryDocumentAndTryLogin();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OktaLoginPage');
    this.oauthService.initImplicitFlow();
  }

}
