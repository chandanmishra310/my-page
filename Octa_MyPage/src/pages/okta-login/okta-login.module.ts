import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OktaLoginPage } from './okta-login';

@NgModule({
  declarations: [
    OktaLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(OktaLoginPage),
  ],
})
export class OktaLoginPageModule {}
