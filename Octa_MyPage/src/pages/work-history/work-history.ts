import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController,NavParams,LoadingController,App} from 'ionic-angular';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import {TabsPage} from '../tabs/tabs';
import {LoginPage} from '../login/login';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { DashboardPage } from '../dashboard/dashboard';



@IonicPage()
@Component({
  selector: 'page-work-history',
  templateUrl: 'work-history.html',
})
export class WorkHistoryPage {
  //OHRID = '302011033';
  maxtime = 20;
  timer: number;
  timeout = 0;
  encodedString: string;
  //OHRID: string = localStorage.getItem("OHRID");
  OHRID:string = localStorage.getItem("emailname");
  myTrainingsCollection: MyAssignmentHistory[] = [];
  myTrainings: MyAssignmentHistoryCollection;
  static baseUrl: string = 'https://genpacticsinstancetest01-a432914.integration.us2.oraclecloud.com/integration/flowapi/rest/';
  static getEmployeeUrl: string = WorkHistoryPage.baseUrl + 'ASSIGNMENT_HISTORY_API/v01/myAssignmentsHistory?OHRID=';
  //static myTrainingUrl: string = WorkHistoryPage.baseUrl + 'ILEARN_MYTRAINING_API/v01/myTrainings?OHRID=';
  static userName = 'rgupta';
  static password = 'O@acle@123';
  constructor(private oauthService: OAuthService,public alertCtrl:AlertController,public app: App,public navCtrl: NavController, public navParams: NavParams, private http: HttpClient,public loadingCtrl: LoadingController) { 
    console.log('ohrid =====>');
    console.log(this.OHRID);
    console.log('navCtrl ==>');
    console.log(navCtrl);
    this.getAuthorizationKey();
    this.getEmployeeUrl();
  }
  goProfile()
{
  this.navCtrl.push(TabsPage);
}
  logout() {
    const confirm = this.alertCtrl.create({
      title: 'MYPAGE',
      message: 'Do you wish to Sign Out?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YES',
          handler: () => {
            this.oauthService.logOut();
            // const root = this.app.getRootNav();
            // root.popToRoot();
            //   this.navCtrl.setRoot(LoginPage);
  
            //   this.navCtrl.popToRoot();
            // console.log('sdffkhkgsdfhsdgfsdgfdsfdsf clicked');
  
          }
        }
      ]
    });
  
    confirm.present();
  }
  getAuthorizationKey() {
    const encodedString = btoa(WorkHistoryPage.userName + ":" + WorkHistoryPage.password);
    console.log('encodedString ==>');
    console.log(encodedString);
    this.encodedString = encodedString;
  }
  getEmployeeUrl() {
   let loader = this.loadingCtrl.create({
      //content: 'Getting latest entries...',
      spinner: 'hide',
      content: '<img src="assets/imgs/logogif.gif"/>',
    });

    // Show the popup
    this.StartTimer(loader);
    loader.present();

    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + this.encodedString)
      .set('AuthUser', '850027873')
      .set('SecretKey', '29fb9858c6eed8ef');
      let result: string[];
    let hh: string;
    let mm: string;

    this.http.get(WorkHistoryPage.getEmployeeUrl+this.OHRID
      , { headers }).subscribe(
        res => {
          this.timeout = 1;
          // console.log("Response-----" +JSON.stringify(res));
          // this.firstName = res.EmployeeDetail.FirstName;
          this.myTrainings = <MyAssignmentHistoryCollection>res;
          console.log('this.myTrainings.MyTrainingsCollection ==>' +JSON.stringify(this.myTrainings));

          console.log(this.myTrainings);

          this.myTrainingsCollection = this.myTrainings.MyAssignmentHistoryCollection.MyAssignmentHistory.map(function (value, index, array) {
           
            // result = value.TotalTrainingHours.split(":");
            // hh = result[0].slice(2, result[0].length);
            // mm = result[1];
            // value.TotalTrainingHours = hh + ':' + mm;

            // result = value.TotalTime.split(":");  
            // hh = result[0].slice(2, result[0].length);
            // mm = result[1];
            // value.TotalTime = hh + ':' + mm;
            return value;
          }, this.myTrainingsCollection);

        },
        err => {
          console.error(err);
        },

        () => {
          
          console.log('Request complete');
          loader.present().then(() => {

            loader.dismiss();
          });
        }
      );
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad WorkHistoryPage');
  }
  StartTimer(loader) {
    this.timer = setTimeout(x => {
      if (this.maxtime <= 0) {
      }
      this.maxtime -= 1;
      if (this.maxtime > 0) {
        this.StartTimer(loader);
        console.log(this.maxtime);
      }
      else {
        if (this.timeout == 0) {
          const root = this.app.getRootNav();
          root.popToRoot();
          this.navCtrl.setRoot(DashboardPage);
          this.navCtrl.popToRoot();
          loader.dismiss();
          // localStorage.setItem("1","timeout")
        }
      }
    }, 1000);
  }
}
export interface MyAssignmentHistory {
  GRADE: string,
  BUSINESS_GROUP_NAME: string,
  PROCESS: string,
  ASSIGNMENT_START_DATE: string,
  ASSIGNMENT_END_DATE:string;
  //Status: string,
  JOB: string,
  SUPERVISOR_NAME: string,
  WORK_LOCATION_CODE: string,
  BASE_LOCATION: string,
}
export interface MyAssignmentHistoryCollection {
  MyAssignmentHistoryCollection: {
    MyAssignmentHistory: MyAssignmentHistory[]
  }
}

